# Changes from M24 to M25

<!-- MACRO{toc} -->

## Applications

* Tikal

    * Fixed default parameter for the default TM resource. Now you can
      just run `tikal -q "text to search"`.
    * Added `-x2` and `-m2` options for
      extraction/merge with new skeleton file.
    * Changed -x and -m to extract/merge with the new skeleton file.
    * Added `-x1` and `-m1` options for
      extraction/merge with original document (similar to previous
      versions. For fully backward-compatible merge you must use M24).
    * Updated Tikal merge function with original file to use the new
      common text-unit merger.
    * Added options for JAR version switch

## Filters Plugin for OmegaT

    * Added basic support for XLIFF 2 documents (under construction).
    * Now target text passed as translation only if it is different
      from the source.
    * Added support for alternate translations (e.g. from XLIFF 1.2
      documents)

## Steps

* Added the TTXSplitter Step

    * It allows to split a given TTX document into several ones with the same source word count.

* Added the TTXJoiner Step

    * It allows to join back TTX documents created with the TTXSplitter Step.

* Consolidated merge steps into: `SkeletonXliffMergerStep`,
  `LegacyXliffMergerStep`, `OriginalDocumentXliffMergerStep` and
  `CombinedXliffMergerStep` classes.

* Rainbow Translation Kit Creation Step

    * Reinstated output for XLIFF v2 using the latest library.
    * Added support for extraction using the new skeleton file.

* Rainbow Translation Kit Merging Step

    * Added basic support for merging XLIFF v2 packages.
    * Added support for merging using the new skeleton file.

* Search and Replace Step

    * Fixed [issue #392](https://bitbucket.org/okapiframework/okapi/issues/392) where the reading of the replacement table
      was trimming all lines. Now replacing or searching for space and
      replacing by nothing works.

## Filters

* XLIFF Filter

    * Changed the reading of `<alt-trans>`
      elements to allow entries with empty `<target>`
      (e.g. some XTM's XLIFF have `<alt-trans>` with
      empty targets).
    * Added the option "Allow modification of existing
      `<alt-trans>` elements"

* YAML Filter

    * Improved support.

* OpenXML Filter

    * Fixed [issue #402](https://bitbucket.org/okapiframework/okapi/issues/402) (Cannot stop the filter before the document
      is done)
    * Fixed [issue #350](https://bitbucket.org/okapiframework/okapi/issues/350) (merge problem when docx has a
      OpenXml.Drawing object)

* PO Filter

    * Fixed issue with `#,` (fuzzy flag) in front of `#~`
      (obsolete) entries.

* JSON Filter

    * Refactored the filter.
    * Fixed [issue #359](https://bitbucket.org/okapiframework/okapi/issues/359) (Need to improve extraction selection)
    * Fixed [issue #373](https://bitbucket.org/okapiframework/okapi/issues/373) (Encoder and `xml:space='preserve'`)
    * Fixed [issue #397](https://bitbucket.org/okapiframework/okapi/issues/397) (Filter not extracting all strings as expected)

## Connectors

* Translate Toolkit TM Connector

    * Updated the parameters API to use `set/getUrl()`
      instead of `set/getHost()` and `set/getPort()`.
    * Updated the default host and port (now obsolete) to `localhost`
      and `8080` to allow local setups to continue to work.
    * Updated the default URL to `https://amagama-live.translatehouse.org/api/v1/`
      (the previous URL is obsolete)

## Libraries

    * Major refactoring of the serialization.
    * Major refactoring of the RawDocument object
    * Updated SWT libraries to 4.3
    * Added lib-tkit library for extraction/merge with skeleton in JSON.
    * Added sort capability to the Filter Configuration common edit dialog.
