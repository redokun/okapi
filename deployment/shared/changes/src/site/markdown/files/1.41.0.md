# Changes from 1.40.0 to 1.41.0

<!-- MACRO{toc} -->

## Applications

* Rainbow

    * Update merger to use `TextUnitMerger`

## Core

    * [Issue #996](https://bitbucket.org/okapiframework/okapi/issues/996): Reference identifier values escaped.

## Filters

* IDML Filter

    * [Issue #1016](https://bitbucket.org/okapiframework/okapi/issues/1016): Hidden pasteboard items extraction capability provided.

* MIF Filter

    * [Issue #965](https://bitbucket.org/okapiframework/okapi/issues/965):
      Font tag names escaped to allow reserved regular expression characters.
    * [Issue #987](https://bitbucket.org/okapiframework/okapi/issues/987):
      Tab, hard return, no-hyphen, soft hyphen hexadecimal representations
      handling improved. Hard return symbols correctly transformed from new
      lines and written back to strings in paragraph lines.
    * [Issue #990](https://bitbucket.org/okapiframework/okapi/issues/990):
      Hard returns can demarcate trans-units in Marker, PgfNumFormat,
      XRefFormat > XRefDef and TextLine statements.
    * [Issue #991](https://bitbucket.org/okapiframework/okapi/issues/991):
      Consequential Strings in XRefs merged with hard returns between them.

* OpenXML Filter

    * [Issue #958](https://bitbucket.org/okapiframework/okapi/issues/958):
      Font mapping for PPTX documents introduced.
    * [Issue #977](https://bitbucket.org/okapiframework/okapi/issues/977):
      The segmentation quality restored for some PPTX documents.
    * [Issue #981](https://bitbucket.org/okapiframework/okapi/issues/981):
      PPTX style definitions minified as much as possible.
    * [Issue #974](https://bitbucket.org/okapiframework/okapi/issues/974):
      Max attribute size conditional parameter provided.
    * [Issue #848](https://bitbucket.org/okapiframework/okapi/issues/848):
      The inserted and deleted table row revisions accepted.
    * [Issue #999](https://bitbucket.org/okapiframework/okapi/issues/999):
      Initial support for the presentation styles hierarchy provided.
    * [Issue #1009](https://bitbucket.org/okapiframework/okapi/issues/1009):
      Support for styles hierarchy in presentation table cells provided.

* Markdown Filter

    * [Issue #890](https://bitbucket.org/okapiframework/okapi/issues/890):
      codes representing inline markup for emphasis, strong emphasis, and links are now
      assigned the ctypes `italic`, `bold`, and `link`
      respectively. Addiitonally, link references are now exposed as paired, rather
      than singleton, tags.

* YAML Filter

    * [Issue #967](https://bitbucket.org/okapiframework/okapi/issues/967):
      Comments after plain scalars preserved.

* XLIFF Filter

    * [Issue #966](https://bitbucket.org/okapiframework/okapi/issues/966):
      New option `subAsTu` allows to extract sub elements as separate text-units.
    * [Issue #688](https://bitbucket.org/okapiframework/okapi/issues/688):
      `CDATA` content can be subfiltered.
    * [Issue #998](https://bitbucket.org/okapiframework/okapi/issues/998):
      Subfiltered sources can be used for overriding empty targets.
    * [Issue #1002](https://bitbucket.org/okapiframework/okapi/issues/1002):
      Subfiltered sources and targets merged correctly.

* XLIFF2 Filter

    * [Issue #966](https://bitbucket.org/okapiframework/okapi/issues/978/enhance-xliff-2-filter-to-add-metadata-to):
      New feature to allow extraction and preservation of xliff2 metadata. Metadata is readonly and cannot be updated
    * [Issue #968](https://bitbucket.org/okapiframework/okapi/issues/968/xliff-2-filter-does-not-convert-translate):
      xliff 2 filter does not convert translate attributes when converting to TextUnit
    * [Issue #969](https://bitbucket.org/okapiframework/okapi/issues/989/merging-xliff2-file-results-in-some-target):
      Merging XLIFF2 file results in some target segments being left out
    * [Issue #939](https://bitbucket.org/okapiframework/okapi/issues/939/xliff2-filter-looses-preserve-whitespace):
      xliff2 filter looses preserve whitespace information
    * [Issue #1007](https://bitbucket.org/okapiframework/okapi/issues/1007/xliff2-filter-has-no-way-to-preserve-ids):
      xliff2 filter has no way to preserve id's and properties/annotations in Ignorable content
    * xliff2 skeleton is now added to an okapi event
    * BETA removed from xliff2 filter name

* PO Filter

    * Increased readaheadlimit buffer to prevent crash with some files. Remove deprecated code for notes

## Libraries

* lib-core

    * [Issue #1003](https://bitbucket.org/okapiframework/okapi/issues/1003/xliff-writer-should-append-x-prefix-for)
      XLIFFContextGroup now adds "x-" prefix automatically for non-standard xliff 1.2 context types.

* lib-merge

    * Complete Refactor of `TextUnitMerger` to better support xliff2 filter and resolve several outstanding bugs
    * `TextUnitMerger` can now be accessed from `OriginalDocumentXliffMergerStep` and Parameters set directly

## Steps

* Format Conversion Step

    * Updated the step to also output XLIFF.

* Simple Word Count Step

    * Fixed the count of the ending part of the segments.

* Rainbow Translation Kit Creation Step

    * Merge now uses `TextUnitMerger` so that merge results are consistent across Okapi
