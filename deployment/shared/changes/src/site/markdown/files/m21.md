# Changes from M20 to M21

<!-- MACRO{toc} -->

## Applications

* General

    * Applications now launch correctly on Mac OS X when they are located in a path containing a space.

* Rainbow

    * Added the `-log` option to specify result log file.
      By default the log file is `{user.home}\rainbowBatchLog.txt`

* Tikal

    * Added the `-safe` option to prompt user when overriding a directory when extracting.

## Filters

* ITS Filters (XML Filter and HTML5+ITS Filter)

    * Continued implementation of ITS 2.0 (See http://okapiframework.org/wiki/wiki/index.php?title=ITS_Components for details)
    * Added support for HTML5 `translate=""` (same as `translate="yes"`).

* XLIFF Filter

    * Continued implementation of ITS 2.0.
    * Added support for `okp:engine` attribute in `<alttrans>`.

* Wiki Filter

    * Fixed [issue #315](https://bitbucket.org/okapiframework/okapi/issues/315): `WikiFilter` didn't work with
      `preserve_whitespace: true`.

* Regex Filter

    * Improved the macStrings default settings to include
      slash+star comments with next extracted string.

* IDML Filter

    * Fixed [issue #316](https://bitbucket.org/okapiframework/okapi/issues/316): Added default to not extract hidden
      layers and added the option "Extract hidden layers".
    * Enabled the option "Create new paragraphs on hard
      returns". \
      **Important: This option is still BETA and may prevent you
      to merge back the extracted file. Make sure to test the
      round-trip before using this option for real projects.**

* TMX Filter

    * Fixed a bug where attribute values on `<tuv>` elements
      were being written back to the skeleton without proper
      escaping.
    * Improved filter performance.

* Properties Filter

    * Fixed [issue #313](https://bitbucket.org/okapiframework/okapi/issues/313) where the extended characters were not
    escaped when using the sub-filter.

* TS Filter

    * Changed the instantiation of the XML parser to use Woodstox.

* XML Stream Filter (Abstract Markup Filter)

    * Fixed [issue #303](https://bitbucket.org/okapiframework/okapi/issues/303): When using the global_pcdata_subfilter
      option, the filter will no longer generate extra segments
      consisting only of placeholders.

## Steps

* Microsoft Batch Translation Step

    * Added support for the `${domain}` variable for
      the category.
    * Added support for `${rootDir}` and other
      variables for the path of the Engine mapping file.

* Quality Check Step

    * Added option to save or not the session. This option is
      not accessible when editing the parameters from CheckMate,
      but only when editing a step's parameters.

* Rainbow Translation Kit Creation Step

    * Added to XLIFF outputs the option of outputting `ctype`
      and `equiv-text` attributes in inline codes.
    * Added to OmegaT output the output of `ctype`
      and ` equiv-text` attributes in inline codes.
    * Added the option to merge a new OmegaT translation kit
      with an existing one, rather than overwriting it.

* Enrycher Step

    * Added support for segmented text units and implemented
      handling of inline codes.
    * Added parameter for number of events to process on each
      call to the service.

* Translation Comparison Step

    * Added option to log the average scores per documents in a
      tab-delimited file.
    * Added the output of a new tab-delimited file with all
      scores, along with the HTML report.
    * Extended the repartition table to use 11 brackets instead
      of 3, and include the two scores.

* Segmentation and Desegmentation Steps

    * Added the option to renumber code IDs after segmentation
      so that they are 1-indexed as much as possible. A
      corresponding option on the desegmentation step reverses the
      process. This option will not work correctly with formats
      that use non-consecutive or non-numeric code IDs, such as
      XLIFF.

## Connectors

* Microsoft Translator Connector

    * Added information about the engine in the query results.

## Libraries

    * Continued implementation of ITS 2.0 in XLIFF Writer.
    * Changed options settings for the XLIFFWriter class to use an
      object rather than multiple setters.

## Filters Plugin for OmegaT

    * Fixed [issue #322](https://bitbucket.org/okapiframework/okapi/issues/322): Updated the TS filter to use the Woodstox
      parser, and added the dependencies.
    * Added the XLIFF Filter to the plug-in.
    * Added basic support for some ITS data categories in the
      Comments pane (Text Analysis, Terminology).
