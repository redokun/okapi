# Changes from M6 to M7

## Applications

* Rainbow

    * Fixed issue where output example was not updated when the top
      input file was removed.
    * Fixed issue where pipeline file was not written as UTF-8.
    * Translation package Creation:
        * Fixed [issue #132](https://bitbucket.org/okapiframework/okapi/issues/132) where we generated segment `<mrk>` in
          XLIFF if the text was pre-translated but not segmented.
    * ID-Based Alignment
        * Implemented [request #134](https://bitbucket.org/okapiframework/okapi/issues/134): A TMX output can now be created for
          un-aligned entries.

## Libraries

* Changed the SVN structure to allow checking-out and building the
  libraries separately from the UI and apps. To get the base libraries
  only: http://okapi.googlecode.com/svn/trunk/okapi.
  To get everything: http://okapi.googlecode.com/svn/trunk.
* Changed the TextContainer class and refactor all dependencies.
  This modification is a major code change.
* Added the `setRootDirectory()` method to the `IQuery`
  interface.
* Updated `QueryManager` to handle empty inline codes and inline codes
  with references when leveraging fuzzy matches
* Added spin-like input part to the generic editor.
* Fixed bug where platform type for "cocoa" was not handled, and
  therefore Mac not detected in some occurrences.
* Added support for ftnsep, ftnsepc, aftnsep and aftnsepc control
  words in the RTF parser, so any defined paragraph or character is
  skipped.
* Added the following generic UI parts to the generic editor:
  `SpinInputPart` and `SeparatorPart`
* The `ScoresAnnotation` class has been deprecated, use the new
  `AltTranslationsAnnotation` instead.
* Fixed help location issue for SRX editor (Ratel).

## Translation resources

* Updated all the connectors for the `IQuery` change and implemented `${rootDir}`
  for all the connectors using locale files: SimpleTM and Pensieve.

* Apertium MT

    * Updated the connector to use the new JSONP API (http://wiki.apertium.org/wiki/Apertium_web_service)
    * Added support for API key (See http://api.apertium.org/register.jsp)

* Cross-Language Gateway MT services

    * Added a new connector for this commercial service.
      See: http://www.crosslang.com/language-professionals/machine-translation/cl-gateway

## Filters

* OpenXML Filter

    * Fixed an issue with open/closing group in some conditions.
    * Fixed an issue with a case of text box resulting in hanging.

* XML Filter

    * Added pre-defined configuration for WiX (Windows Installer
      XML) Localization files.
    * Improved handling of empty elements.

* XLIFF Filter

    * Improved the reading of pre-segmented content, so the segment
      Ids are now preserved instead of re-generated.
    * Fixed parent-id for StartSubDocument event/resource.
    * Implemented read-only property for build-num in `<file>`
      and extradata in `<trans-unit>`.
    * Improved support for segmentation choices in output. Now the
      filter can remove, add or keep the segmentation for each
      trans-unit.

* Vignette Filter

    * Fixed issue of 64K limit of blocks (due to Java
      DataOutputStream writeUTF() limitation): added multi-chunks
      write/read function.

* Ruby on Rails YAML Filter

    * Added support for Ruby on Rails YAML filter. It offers partial
      support of [YAML files](http://www.yaml.org/).

* Versified Text Filter

    * Added support for filter on versified text documents.

* HTML Filter

    * Fixed default configurations to extract ALT attribute of AREA
      elements.

* TMX Filter

    * Fixed the bug where the option "escape greater-than
      characters" was not working.

## Steps

* Implemented `${rootDir}` for the follwoing steps: Format Conversion,
  Generate SimpleTM, Segmentation, TM Import, Leveraging, Batch
  Translation.

* Segmentation Step

    * Made copy of source into empty target an option.
    * Added the option of verifying source and target segments match
      after segmentation.

* Added the "Diff Leverage" Step

* Added the "External Command" Step

* Sentence Alignment Step

    * Added support to use a single bilingual input file.

* Format Conversion Step

    * Added the option to generate output files with automated
      extension.

* Text Modification Step

    * Implemeted [Request #100](https://bitbucket.org/okapiframework/okapi/issues/100): An option to modify or not entries
      without text.

