package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.roundtrip.integration.RoundTripMarkdownIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripSimplifyHtmlIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_html";
	private static final String DIR_NAME = "/html/";
	private static final List<String> EXTENSIONS = Arrays.asList(".html", ".htm", ".xhtml");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".simplify_xliff";
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = HtmlFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripMarkdownIT.class);

	public RoundTripSimplifyHtmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, FILTER_CONSTRUCTOR);
		addKnownFailingFile("98959751.html");
		addKnownFailingFile("ugly_big.htm");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/html/simple_font_size.html").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, null, null,
				new FileComparator.Utf8FilePerLineComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void debug2() {
		final File file = root.in("/html/xhtml/chapter01.xhtml").asFile();
		runTest(new TestJob("okf_html@xhtml", false, file, null,
				file.getParent(), new FileComparator.Utf8FilePerLineComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void htmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}

	@Test
	public void htmlFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}
}
