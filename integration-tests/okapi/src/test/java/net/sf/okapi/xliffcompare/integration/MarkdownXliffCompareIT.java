package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.markdown.MarkdownFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class MarkdownXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_markdown";
	private static final String DIR_NAME = "/markdown/";
	private static final List<String> EXTENSIONS = Arrays.asList(".md");

	public MarkdownXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, MarkdownFilter::new);
	}

	@Test
	public void markdownXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
