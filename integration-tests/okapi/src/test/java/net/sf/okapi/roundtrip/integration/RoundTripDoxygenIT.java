package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.doxygen.DoxygenFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripDoxygenIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_doxygen";
	private static final String DIR_NAME = "/doxygen/";
	private static final List<String> EXTENSIONS = Arrays.asList(".h", ".py");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = DoxygenFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripDoxygenIT.class);

	public RoundTripDoxygenIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);
		if ("\r\n".equals(System.lineSeparator())) {
			addKnownFailingFile("javadoc-style.h");
			addKnownFailingFile("sample.h");
			addKnownFailingFile("python.h");
		}
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/doxygen/lists.h").asFile();
		runTest(new TestJob(CONFIG_ID, false, file, null, null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void doxygenFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void doxygenFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(CONFIG_ID, false, new FileComparator.EventComparator());
	}
}
