package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.mif.MIFFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripMifIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_mif";
	private static final String DIR_NAME = "/mif/";
	private static final List<String> EXTENSIONS = Arrays.asList(".mif");

	public RoundTripMifIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, MIFFilter::new);
	}

	@Test
	public void mifFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void mifSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
