package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.transtable.TransTableFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class TranstableXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_transtable";
	private static final String DIR_NAME = "/transtable/";
	private static final List<String> EXTENSIONS = Arrays.asList(".txt");

	public TranstableXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TransTableFilter::new);
	}

	@Test
	public void transtableXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
