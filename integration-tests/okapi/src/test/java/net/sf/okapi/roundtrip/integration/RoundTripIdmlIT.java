package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.idml.IDMLFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripIdmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_idml";
	private static final String DIR_NAME = "/idml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".idml");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = IDMLFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripIdmlIT.class);

	public RoundTripIdmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, IDMLFilter::new);
		addKnownFailingFile("sample_brochure2.idml");
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(EXTENSIONS);
		final File file =
				root.in("/idml/small_test.idml").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, null, null,
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void idmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}
	@Test
	public void idmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
