package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.xml.XMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripItsIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_xml";
	private static final String DIR_NAME = "/its/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xml");

	public RoundTripItsIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XMLFilter::new);
	}

	@Test
	public void itsXmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void itsXmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
