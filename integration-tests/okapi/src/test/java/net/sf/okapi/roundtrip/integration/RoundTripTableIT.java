package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.table.TableFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripTableIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_table";
	private static final String DIR_NAME = "/table/";
	private static final List<String> EXTENSIONS = Arrays.asList(".csv", ".tab");

	public RoundTripTableIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TableFilter::new);
	}

	@Test
	public void tableFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void tableSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
