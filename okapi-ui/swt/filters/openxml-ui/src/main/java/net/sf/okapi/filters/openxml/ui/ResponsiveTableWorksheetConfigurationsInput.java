/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.common.ui.ResponsiveTable;
import net.sf.okapi.filters.openxml.WorksheetConfiguration;
import net.sf.okapi.filters.openxml.WorksheetConfigurations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

final class ResponsiveTableWorksheetConfigurationsInput implements WorksheetConfigurations.Input {
    private static final String EMPTY = "";
    private final ResponsiveTable responsiveTable;
    private List<WorksheetConfiguration> worksheetConfigurations;

    ResponsiveTableWorksheetConfigurationsInput(final ResponsiveTable responsiveTable) {
        this.responsiveTable = responsiveTable;
    }

    @Override
    public Iterator<WorksheetConfiguration> read() {
        if (null != this.worksheetConfigurations) {
            return Collections.emptyIterator();
        }
        this.worksheetConfigurations = new LinkedList<>();
        Arrays.stream(this.responsiveTable.rows()).forEach(i -> {
            final TableItemWorksheetConfiguration c = new TableItemWorksheetConfiguration(i);
            c.matches(EMPTY); // force data reading
            this.worksheetConfigurations.add(c);
        });
        return this.worksheetConfigurations.iterator();
    }
}

