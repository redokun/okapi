/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml.ui;

import net.sf.okapi.filters.openxml.WorksheetConfiguration;
import org.eclipse.swt.widgets.TableItem;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

final class TableItemWorksheetConfiguration implements WorksheetConfiguration {
    static final String DELIMITER = ",";
    private static final String DELIMITING_EXPRESSION = "\\s*" + DELIMITER + "\\s*";

    private final TableItem tableItem;
    private WorksheetConfiguration worksheetConfiguration;
    private boolean read;

    TableItemWorksheetConfiguration(final TableItem tableItem) {
        this.tableItem = tableItem;
    }

    @Override
    public boolean matches(final String worksheetName) {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.matches(worksheetName);
    }

    @Override
    public Set<Integer> excludedRows() {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.excludedRows();
    }

    @Override
    public Set<String> excludedColumns() {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.excludedColumns();
    }

    @Override
    public Set<Integer> metadataRows() {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.metadataRows();
    }

    @Override
    public Set<String> metadataColumns() {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.metadataColumns();
    }

    /**
     * Reads worksheet configuration values from a table item.
     * <p>
     * The following values format is supported:
     * [namePattern]
     * [excludedRows]
     * [excludedColumns]
     * [metadataRows]
     * [metadataColumns]
     * <p>
     * The name pattern can be any supported regular expression.
     * <p>
     * The excluded rows can be a set of integer values delimited by
     * {@link TableItemWorksheetConfiguration#DELIMITER}.
     * <p>
     * The excluded columns can be a set of string values delimited by
     * {@link TableItemWorksheetConfiguration#DELIMITER}.
     */
    private void fromTableItem() {
        this.worksheetConfiguration = new WorksheetConfiguration.Default(
            this.tableItem.getText(0),
            Arrays.stream(
                this.tableItem.getText(1)
                    .split(DELIMITING_EXPRESSION) // returns a non empty array for an empty string
            )
            .filter(s -> !s.isEmpty())
            .map(s -> Integer.parseUnsignedInt(s))
            .collect(Collectors.toList()),
            Arrays.stream(
                this.tableItem.getText(2)
                    .split(DELIMITING_EXPRESSION)
            )
            .filter(s -> !s.isEmpty())
            .collect(Collectors.toList()),
            Arrays.stream(
                this.tableItem.getText(3)
                    .split(DELIMITING_EXPRESSION)
            )
            .filter(s -> !s.isEmpty())
            .map(s -> Integer.parseUnsignedInt(s))
            .collect(Collectors.toList()),
            Arrays.stream(
                this.tableItem.getText(4)
                    .split(DELIMITING_EXPRESSION)
            )
            .filter(s -> !s.isEmpty())
            .collect(Collectors.toList())
        );
        this.read = true;
    }

    @Override
    public <T> T writtenTo(final Output<T> output) {
        if (!this.read) {
            fromTableItem();
        }
        return this.worksheetConfiguration.writtenTo(output);
    }
}

