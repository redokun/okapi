package net.sf.okapi.steps.xliffsplitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import net.sf.okapi.common.BOMAwareInputStream;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.Util;

import net.sf.okapi.common.XMLFileCompare;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

@RunWith(JUnit4.class)
public class XliffWCSplitterTest {

	private FileLocation root;
	private String splitDir;
	
	@Before
	public void setUp() throws Exception {
		root = FileLocation.fromClass(getClass());
		splitDir = root.out("split-wc").toString() + "/";
		Util.createDirectories(splitDir);
	}
	
	@Test
	public void test5Words ()
		throws IOException
	{
		XliffWCSplitterParameters p = new XliffWCSplitterParameters();
		p.setThreshold(5);
		XliffWCSplitter spltr = new XliffWCSplitter(p);
		try ( InputStream is = root.in("input1.xlf").asInputStream() ) {
			Util.deleteDirectory(splitDir, true);
			Map<String, Integer> res = spltr.process(is, splitDir+"output5w", "en-US");
			assertEquals(3, res.size());
			assertEquals(7, (int)res.get("output5w_PART001.xlf"));
			assertEquals(8, (int)res.get("output5w_PART002.xlf"));
			assertEquals(0, (int)res.get("output5w_PART003.xlf"));
		}
	}

	@Test
	public void test3Words ()
		throws IOException
	{
		XliffWCSplitterParameters p = new XliffWCSplitterParameters();
		p.setThreshold(3);
		XliffWCSplitter spltr = new XliffWCSplitter(p);
		try ( InputStream is = root.in("input1.xlf").asInputStream() ) {
			Util.deleteDirectory(splitDir, true);
			Map<String, Integer> res = spltr.process(is, splitDir+"output3w", "en-US");
			assertEquals(5, res.size());
			assertEquals(4, (int)res.get("output3w_PART001.xlf"));
			assertEquals(3, (int)res.get("output3w_PART002.xlf"));
			assertEquals(4, (int)res.get("output3w_PART003.xlf"));
			assertEquals(4, (int)res.get("output3w_PART004.xlf"));
			assertEquals(0, (int)res.get("output3w_PART005.xlf"));
		}
	}

	@Test
	public void test9999Words ()
		throws IOException
	{
		XliffWCSplitterParameters p = new XliffWCSplitterParameters();
		p.setThreshold(9999);
		XliffWCSplitter spltr = new XliffWCSplitter(p);
		try ( InputStream is = root.in("input1.xlf").asInputStream() ) {
			Util.deleteDirectory(splitDir, true);
			Map<String, Integer> res = spltr.process(is, splitDir+"output9999w", "en-US");
			assertEquals(1, res.size());
			assertEquals(15, (int)res.get("output9999w_PART001.xlf"));
		}
	}

	@Test
	public void contextGroupsCopiedOnSplitting() throws Exception {
		Logger logger = LoggerFactory.getLogger(getClass());
		Path input = root.in("context-group").asPath();
		Path output = Path.of(splitDir);
		XliffWCSplitterParameters p = new XliffWCSplitterParameters();
		p.setThreshold(2);
		XliffWCSplitter spltr = new XliffWCSplitter(p);
		try (InputStream is = input.resolve("context-group.xlf").toUri().toURL().openStream()) {
			Util.deleteDirectory(output.toString(), true);
			Map<String, Integer> res = spltr.process(is, output + "/out", "en-US");
			assertEquals(3, res.size());
			assertEquals(2, (int)res.get("out_PART001.xlf"));
			assertEquals(2, (int)res.get("out_PART002.xlf"));
		}
		final XmlDocumentsComparison dc = new XmlDocumentsComparison(input, output, logger);
		dc.compareWithGold(output.toString(), "out_PART001.xlf", "context-group_PART001.xlf");
		dc.compareWithGold(output.toString(), "out_PART002.xlf", "context-group_PART002.xlf");
	}
}
