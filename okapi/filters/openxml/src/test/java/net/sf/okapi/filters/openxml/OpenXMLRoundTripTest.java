/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.ParametersString;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.fontmappings.DefaultFontMapping;
import net.sf.okapi.common.filters.fontmappings.DefaultFontMappings;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.RawDocument;

import net.sf.okapi.common.resource.TextContainer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * This tests OpenXMLFilter (including OpenXMLContentFilter) and
 * OpenXMLZipFilterWriter (including OpenXMLContentSkeleton writer)
 * by filtering, automatically translating, and then writing the
 * zip file corresponding to a Word, Excel or Powerpoint 2009 file, 
 * then comparing it to a gold file to make sure nothing has changed.
 * It does this with a specific list of files.
 * 
 * <p>This is done with no translator first, to make sure the same
 * file is created that was filtered in the first place.  Then it
 * is translated into Pig Latin by PigLatinTranslator, translated so
 * codes are expanded by CodePeekTranslator, and then translated to
 * see a view like the translator will see by TagPeekTranslator.
 */

@RunWith(JUnit4.class)
public class OpenXMLRoundTripTest extends AbstractOpenXMLRoundtripTest {
	private LocaleId locENUS = LocaleId.fromString("en-us");

	private FileLocation root;

	@Before
	public void before() throws Exception {
		this.allGood = true;
		this.root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testHiddenTablesWithFormula() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setTranslateExcelHidden(false);
		runOneTest("hidden_table_with_formula.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testHiddenMergeCells() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setTranslateExcelHidden(false);
		runOneTest("HiddenMergeCells.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testPhoneticRunPropertyForAsianLanguages() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setTranslateExcelHidden(false);
		runOneTest("japanese_phonetic_run_property.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testExternalHyperlinks() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setExtractExternalHyperlinks(true);
		runOneTest("external_hyperlink.docx", true, false, cparams);
		runOneTest("external_hyperlink.pptx", true, false, cparams);
		runOneTest("external_hyperlink.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testClarifiablePart() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();

		runOneTest("clarifiable-part-en.pptx", false, false, conditionalParameters, "", LocaleId.ENGLISH, LocaleId.ENGLISH);
		runOneTest("clarifiable-part-ar.pptx", false, false, conditionalParameters, "", LocaleId.ARABIC, LocaleId.ARABIC);
		runOneTest("clarifiable-part-en.xlsx", false, false, conditionalParameters, "", LocaleId.ENGLISH, LocaleId.ENGLISH);
		runOneTest("clarifiable-part-ar.xlsx", false, false, conditionalParameters, "", LocaleId.ARABIC, LocaleId.ARABIC);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void doesNotAcceptRevisions() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();
		conditionalParameters.setAutomaticallyAcceptRevisions(false);

		runOneTest("numbering-revisions.docx", false, false, conditionalParameters);
		runOneTest("table-grid-revisions.docx", false, false, conditionalParameters);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void acceptsRevisionsInComplexFields() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();

		runOneTest("768.docx", false, false, conditionalParameters);
		runOneTest("768-2.docx", false, false, conditionalParameters);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void acceptsMovedContentRevisions() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();

		runOneTest("843-1.docx", false, false, conditionalParameters);
		runOneTest("843-2.docx", false, false, conditionalParameters);
		runOneTest("843-31.docx", false, false, conditionalParameters);
		runOneTest("843-32.docx", false, false, conditionalParameters);
		runOneTest("843-33.docx", false, false, conditionalParameters);
		runOneTest("843-34.docx", false, false, conditionalParameters);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void acceptsDeletedParagraphMarkRevision() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();
		runOneTest("847-1.docx", false, false, conditionalParameters);
		runOneTest("847-2.docx", false, false, conditionalParameters);
		runOneTest("847-3.docx", false, false, conditionalParameters);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void insertedAndDeletedTableRowRevisionsAccepted() {
		runOneTest("848.docx", false, false, new ConditionalParameters());
		runOneTest("848-nested-tables-with-revisions.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void nestedTablesWithoutRevisionsRoundTripped() {
		runOneTest("848-nested-tables.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void crossStructureRevisionsInTablesAccepted() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("1080-1.docx", false, false, conditionalParameters);
		runOneTest("1080-2.docx", false, false, conditionalParameters);
		runOneTest("1080-3.docx", false, false, conditionalParameters);
		runOneTest("1080-4.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void excelDocumentRevisionsAccepted() {
		final ConditionalParameters cp = new ConditionalParameters();
		runOneTest("983-1a.xlsx", false, false, cp);
		runOneTest("983-2a.xlsx", false, false, cp);
		runOneTest("983-3.xlsx", false, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void tablesWithEmptyLastRowsHandled() {
		runOneTest("1095.pptx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void runTestsWithColumnExclusion() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
		params.worksheetConfigurations().addFrom(
			new ParametersStringWorksheetConfigurationsInput(
				new ParametersString(
				"worksheetConfigurations.0.namePattern=Sheet1\n" +
					"worksheetConfigurations.0.excludedColumns=A\n" +
					"worksheetConfigurations.number.i=1"
				)
			)
		);
        runOneTest("shared_string_in_two_columns.xlsx", true, false, params);
        assertTrue("Some Roundtrip files failed.", allGood);
	}

	// Slimmed-down version of some of the integration tests -- this checks for idempotency
	// by roundtripping once, then using the output of that to roundtrip again.  The first and
	// second roundtrip outputs should be the same.
	@Test
	public void runTestTwice() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
		runTestTwice("Escapades.docx", params);
	}

	@Test
	public void runTestsExcludeGraphicMetaData() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateWordExcludeGraphicMetaData(true);
		runTests("exclude_graphic_metadata/", params,
				"textarea.docx",
				"picture.docx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsWithAggressiveTagStripping() {
		ConditionalParameters params = new ConditionalParameters();
		params.setCleanupAggressively(true);
		runTests("aggressive/", params,
				 "spacing.docx",
				 "vertAlign.docx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsWithHiddenCellsExposed() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateExcelHidden(true);
		runTests("hidden_cells/", params, "hidden_cells.xlsx");
		runTests("hidden_cells/", params, "hidden_stuff.xlsx");
		runTests("hidden_cells/", params, "hidden_table.xlsx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestWithStyledTextCell() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateExcelHidden(true);
		runOneTest("styled_cells.xlsx", true, false, params);
	}

	@Test
	public void runTestsWithTextfield() {
		ConditionalParameters cparams = new ConditionalParameters();
		cparams.tsComplexFieldDefinitionsToExtract.add("FORMTEXT");

		runOneTest("Textfield.docx", true, false, cparams, "textfield");
		runOneTest("ComplexTextfield.docx", true, false, cparams, "textfield");
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void roundTripsNestedContent() throws Exception {
		runOneTest("798.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundTripsWithRefinedComplexFieldsEndBoundaries() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.tsComplexFieldDefinitionsToExtract.add("COMMENTS");
		conditionalParameters.tsComplexFieldDefinitionsToExtract.add("TITLE");

		runOneTest("830-1.docx", false, false, conditionalParameters);
		runOneTest("830-2.docx", false, false, conditionalParameters);
		runOneTest("830-3.docx", false, false, conditionalParameters);
		runOneTest("830-4.docx", false, false, conditionalParameters);
		runOneTest("830-5.docx", false, false, conditionalParameters);
		runOneTest("830-7.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void complexFieldsMultipleInstructionsHandled() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("1083-hyperlink-and-date-instructions.docx", false, false, conditionalParameters);
		runOneTest("1083-date-and-hyperlink-instructions.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void roundTripsWithStructuralDocumentTags() throws Exception {
		runOneTest("834.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundTripsWithReorderedNotesAndComments() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.setReorderPowerpointNotesAndComments(true);
		runOneTest("835.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void roundTripsInStrictMode() throws Exception {
		runOneTest("858.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundTripsWithOptimisedWordProcessingStyles() throws Exception {
		runOneTest("853-all-common.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundTripsWithClarifiedBidiFormattingInStyles() throws Exception {
		runOneTest("899.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void powerpointBidiFormattingConsidered() throws Exception {
		runOneTest("927-bodypr-rtlcol-1.pptx", false, false, new ConditionalParameters(), "", LocaleId.ARABIC, LocaleId.ENGLISH);
		runOneTest("927-bodypr-rtlcol-0.pptx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		runOneTest("927-presentation-rtl-1.pptx", false, false, new ConditionalParameters(), "", LocaleId.ARABIC, LocaleId.ENGLISH);
		runOneTest("927-presentation-rtl-0.pptx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		runOneTest("927-tblpr-rtl-1.pptx", false, false, new ConditionalParameters(), "", LocaleId.ARABIC, LocaleId.ENGLISH);
		runOneTest("927-tblpr-rtl-0.pptx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		runOneTest("927-p-ppr-rtl-1.pptx", false, false, new ConditionalParameters(), "", LocaleId.ARABIC, LocaleId.ENGLISH);
		runOneTest("927-p-ppr-rtl-0.pptx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		runOneTest("927-r-rpr-rtl.pptx", false, false, new ConditionalParameters(), "", LocaleId.HEBREW, LocaleId.ENGLISH);
		runOneTest("927-r-rpr-no-rtl.pptx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.HEBREW);
		assertTrue(allGood);
	}

	/**
	 * Runs tests for all given files.
	 *
	 * @param files file names
	 */
	private void runTests(String goldSubDirPath, ConditionalParameters params, String... files) {
		for(String s : files)
		{
			runOneTest(s, true, false, params, goldSubDirPath);  // PigLatin
		}
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsAddLineSeparatorCharacter() {
		ConditionalParameters params = new ConditionalParameters();
		params.setAddLineSeparatorCharacter(true);

		List<String> files = new ArrayList<>();
		files.add("Document-with-soft-linebreaks.docx");
		files.add("Document-with-soft-linebreaks.pptx");
		files.add("PageBreak.docx");
		files.add("1058.pptx");

		runTests("lbaschar/", params, files.toArray(new String[0]));
	}

	@Test
	public void testAdditionalDocumentTypes() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();
		conditionalParameters.setTranslateExcelSheetNames(true);

		runOneTest("macro-2.docm", true, false, conditionalParameters);

		runOneTest("template-2.dotx", true, false, conditionalParameters);
		runOneTest("macro-template-2.dotm", true, false, conditionalParameters);

		runOneTest("macro-2.pptm", true, false, conditionalParameters);

		runOneTest("show-2.ppsx", true, false, conditionalParameters);
		runOneTest("macro-show-2.ppsm", true, false, conditionalParameters);

		runOneTest("template-2.potx", true, false, conditionalParameters);
		runOneTest("macro-template-2.potm", true, false, conditionalParameters);

		runOneTest("macro-2.xlsm", true, false, conditionalParameters);

		runOneTest("template-2.xltx", true, false, conditionalParameters);
		runOneTest("macro-template-2.xltm", true, false, conditionalParameters);

		runOneTest("2-pages.vsdx", true, false, conditionalParameters);
		runOneTest("2-pages.vsdm", true, false, conditionalParameters);

		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void testMultilineFormula() throws Exception {
		runOneTest("multiline_formula.xlsx", true, false, new ConditionalParameters());
		assertTrue("Roundtrip file failed.", allGood);
	}

	@Test
	public void doesNotCrashWithEmptyParagraphLevelsInNotesStyles() {
		runOneTest("794.pptx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundtripsWithStyleOptimisationApplied() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("formatting/803-1.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("formatting/803-2.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("formatting/803-oo.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("formatting/803-defrprs-and-no-rprs.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("formatting/803-defrprs-and-rprs.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("853-all-common.docx", false, false, conditionalParameters);
		runOneTest("884.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void roundtripsWithAggressiveCleanup() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.setCleanupAggressively(true);
		runOneTest("formatting/823.pptx", false, false, conditionalParameters, "formatting/");
		runOneTest("formatting/complexScript.docx", false, false, conditionalParameters, "formatting/");
		assertTrue(allGood);
	}

	@Test
	public void doesNotCrashOnRequesting0ParagraphLevel() {
		runOneTest("882.pptx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundtripsWithRunFontsHintRespect() {
		runOneTest("851.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void roundtripsWithRunFontsDifferences() {
		runOneTest("888.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void documentWithRtlLanguageIsMerged() {
		runOneTest("930.docx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		assertTrue(allGood);
	}

	@Test
	public void secondDocumentWithRtlLanguageIsMerged() {
		runOneTest("992.docx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		assertTrue(allGood);
	}

	@Test
	public void doesNotCrashOnMerging() {
		runOneTest("956.docx", false, false, new ConditionalParameters(), "", LocaleId.ENGLISH, LocaleId.ARABIC);
		assertTrue(allGood);
	}

	@Test
	public void fontMappingsAppliedInWordDocuments() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.fontMappings(
			new DefaultFontMappings(
				new DefaultFontMapping(".*", ".*", "Times.*", "Arial")
			)
		);
		runOneTest("937-1.docx", false, false, conditionalParameters);
		runOneTest("937-2.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void fontMappingsAppliedInPresentationDocuments() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.fontMappings(
			new DefaultFontMappings(
				new DefaultFontMapping(".*", ".*", "Times.*", "Arial")
			)
		);
		runOneTest("958-1.pptx", false, false, conditionalParameters);
		conditionalParameters.fontMappings(
			new DefaultFontMappings(
				new DefaultFontMapping(".*", ".*", "Arial", "Times New Roman"),
				new DefaultFontMapping(".*", ".*", "\\+mn.*", "Times New Roman")
			)
		);
		runOneTest("958-2.pptx", false, false, conditionalParameters);
		runOneTest("958-3.pptx", false, false, conditionalParameters);
		runOneTest("958-4.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void fontMappingsAppliedInSpreadsheetDocuments() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.fontMappings(
			new DefaultFontMappings(
				new DefaultFontMapping(".*", ".*", "Times.*", "Arial")
			)
		);
		runOneTest("972-shared-strings-and-comments.xlsx", false, false, conditionalParameters);
		runOneTest("972-styles.xlsx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void runPropertiesMinified() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("948-1.docx", false, false, conditionalParameters);
		runOneTest("948-2.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	/**
	 * @todo #948: move to {@link OpenXMLRoundTripTest#runPropertiesMinified()}
	 *  when the SpreadsheetML restriction is removed
	 */
	@Test
	public void runPropertiesNotMinified() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("948-3.xlsx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void runContainersConsideredForStylesOptimisation() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("952-1.docx", false, false, conditionalParameters);
		runOneTest("952-2.docx", false, false, conditionalParameters);
		runOneTest("952-3.docx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void powerpointStylesHierarchyConsidered() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("999.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-body-style.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-body-style-override.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-title-style.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-title-style-override.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-lst-style.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-master-lst-style-override.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-layout-title-lst-style.pptx", false, false, conditionalParameters);
		runOneTest("999-slide-layout-title-lst-style-override.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void powerpointTableStylesConsidered() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("1009-1.pptx", false, false, conditionalParameters);
		runOneTest("1009-2.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void powerpointExcludedAndHiddenPartsAvailableForModifications() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setPowerpointIncludedSlideNumbersOnly(true);
		cp.tsPowerpointIncludedSlideNumbers.add(2);
		cp.fontMappings(
			new DefaultFontMappings(
				new DefaultFontMapping(".*", ".*", "Times.*", "Arial")
			)
		);
		runOneTest("1011-slide1-visible-slide2-hidden.pptx", false, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void okapiMarkersPreserved() {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		runOneTest("OkapiMarkers.docx", false, false, conditionalParameters);
		runOneTest("OkapiMarkers.xlsx", false, false, conditionalParameters);
		runOneTest("OkapiMarkers.pptx", false, false, conditionalParameters);
		assertTrue(allGood);
	}

	@Test
	public void groupsOfWorksheetsAndRowsProvided() {
		runOneTest("1059.xlsx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void inlineStringsTransformedToSharedStrings() {
		runOneTest("982.xlsx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void sheetNamesSyncedWithTranslations() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setTranslateExcelSheetNames(true);
		runOneTest("1051-cross-sheets-references.xlsx", true, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void tableAndPivotalTableColumnNamesSyncedWithTranslations() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setTranslateExcelSheetNames(true);
		runOneTest("1051-cross-sheets-table-references.xlsx", true, false, cp);
		runOneTest("1051-cross-sheets-table-references-2.xlsx", true, false, cp);
		runOneTest("1051-cross-sheets-table-references-2-hidden.xlsx", true, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void paragraphsWithAbsentPropertiesMerged() {
		runOneTest("1102.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void dispersedTranslationsContextualised() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setTranslateExcelSheetNames(true);
		runOneTest("1108-1.xlsx", true, false, cp);
		runOneTest("1108-2.xlsx", true, false, cp);
		runOneTest("1108-3.xlsx", true, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void differentialFormatReadingClarified() {
		runOneTest("1144.xlsx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void cellReferencesRangePartsInitialisationClarified() {
		runOneTest("1143.xlsx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void emptyReferentRunsHandlingClarified() {
		final ConditionalParameters cp = new ConditionalParameters();
		final LocaleId sourceLocale = LocaleId.ENGLISH;
		final LocaleId targetLocale = LocaleId.FRENCH;
		final Translation translation = new Translation.Copy(targetLocale);
		try (
			final IFilter filter = new OpenXMLFilter();
			final IFilterWriter writer = filter.createFilterWriter()
		) {
			filter.setParameters(cp);
			try {
				filter.open(
					new RawDocument(
						root.in("/1157.docx").asUrl().toURI(),
						OpenXMLFilter.ENCODING.name(),
						sourceLocale,
						targetLocale
					),
					true
				);
			} catch (Exception e) {
				throw new OkapiException(e);
			}
			writer.setParameters(cp);
			writer.setOptions(targetLocale, OpenXMLFilter.ENCODING.name());
			writer.setOutput(root.out("/Out1157.docx").asPath().toString());
			while (filter.hasNext()) {
				final Event event = filter.next();
				if (event.isTextUnit()) {
					translation.applyTo(event);
					final TextContainer tc = event.getTextUnit().getTarget(targetLocale);
					if (!tc.hasCode() && !tc.hasText(true)) {
						tc.append("new");
					}
				}
				writer.handleEvent(event);
			}
		}
	}

	@Test
	public void complexScriptPropertiesCleared() {
		runOneTest("947-non-cs.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void nonComplexScriptPropertiesCleared() {
		runOneTest("947-cs.docx", false, false, new ConditionalParameters(), "", LocaleId.ARABIC, LocaleId.ARABIC);
		assertTrue(allGood);
	}

	@Test
	public void nonComplexScriptClearedAndComplexScriptPropertiesRemained() {
		runOneTest("947-non-cs-and-cs.docx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void nonComplexScriptClearedAndComplexScriptPropertiesRemained2() throws Exception {
		final ConditionalParameters cp = new ConditionalParameters();
		final LocaleId sourceLocale = LocaleId.ENGLISH;
		final LocaleId targetLocale = LocaleId.ARABIC;
		final Path inputPath = root.in("/947-non-cs.docx").asPath();
		final Path goldPath = root.in("/gold/Out947-non-cs-remained.docx").asPath();
		final Path outputPath = root.out("/Out947-non-cs-remained.docx").asPath();
		try (
			final IFilter filter = new OpenXMLFilter();
			final IFilterWriter writer = filter.createFilterWriter()
		) {
			filter.setParameters(cp);
			try {
				filter.open(
					new RawDocument(
						inputPath.toUri(),
						OpenXMLFilter.ENCODING.name(),
						sourceLocale,
						targetLocale
					),
					true
				);
			} catch (Exception e) {
				throw new OkapiException(e);
			}
			writer.setParameters(cp);
			writer.setOptions(targetLocale, OpenXMLFilter.ENCODING.name());
			writer.setOutput(outputPath.toString());
			while (filter.hasNext()) {
				final Event event = filter.next();
				if (event.isTextUnit()) {
					event.getTextUnit().setTarget(
						targetLocale,
						new TextContainer("واحد")
					);
				}
				writer.handleEvent(event);
			}
			final OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(
				Files.newInputStream(goldPath),
				Files.newInputStream(outputPath)
			);
			boolean same = differ.isIdentical();
			if (!same) {
				LOGGER.warn("{}{}", inputPath.getFileName(), " FAILED");
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			differ.cleanup();
			assertTrue(same);
		}
	}

	@Test
	public void textFormulaRecalculationPerformedOnSheetLoading() throws Exception {
		final ConditionalParameters cp = new ConditionalParameters();
		final LocaleId sourceLocale = LocaleId.ENGLISH;
		final LocaleId targetLocale = LocaleId.FRENCH;
		final Path inputPath = root.in("/1141.xlsx").asPath();
		final Path goldPath = root.in("/gold/Out1141.xlsx").asPath();
		final Path outputPath = root.out("/Out1141.xlsx").asPath();
		try (
			final IFilter filter = new OpenXMLFilter();
			final IFilterWriter writer = filter.createFilterWriter()
		) {
			filter.setParameters(cp);
			try {
				filter.open(
					new RawDocument(
						inputPath.toUri(),
						OpenXMLFilter.ENCODING.name(),
						sourceLocale,
						targetLocale
					),
					true
				);
			} catch (Exception e) {
				throw new OkapiException(e);
			}
			writer.setParameters(cp);
			writer.setOptions(targetLocale, OpenXMLFilter.ENCODING.name());
			writer.setOutput(outputPath.toString());
			while (filter.hasNext()) {
				final Event event = filter.next();
				if (event.isTextUnit() && "A text".equals(event.getTextUnit().getSource().getCodedText())) {
					event.getTextUnit().setTarget(
						targetLocale,
						new TextContainer("Changed text")
					);
				}
				writer.handleEvent(event);
			}
			final OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(
				Files.newInputStream(goldPath),
				Files.newInputStream(outputPath)
			);
			boolean same = differ.isIdentical();
			if (!same) {
				LOGGER.warn("{}{}", inputPath.getFileName(), " FAILED");
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			differ.cleanup();
			assertTrue(same);
		}
	}

	@Test
	public void valuesFromCellsOfStringTypeWithEmptyFormulasTreatedAsInlineStrings() {
		runOneTest("1116.xlsx", false, false, new ConditionalParameters());
		assertTrue(allGood);
	}

	@Test
	public void breakReplacementsInFieldsWithParagraphsClarified() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setAddLineSeparatorCharacter(true);
		runOneTest("1172.docx", false, false, cp);
		assertTrue(allGood);
	}

	@Test
	public void textRenderingClarifiedForRTLDirection() throws Exception {
		final ConditionalParameters cp = new ConditionalParameters();
		final LocaleId sourceLocale = LocaleId.ENGLISH;
		final LocaleId targetLocale = LocaleId.ARABIC;
		final Path inputPath = root.in("/1127-1.pptx").asPath();
		final Path goldPath = root.in("/gold/Out1127-1.pptx").asPath();
		final Path outputPath = root.out("/Out1127-1.pptx").asPath();
		try (
			final IFilter filter = new OpenXMLFilter();
			final IFilterWriter writer = filter.createFilterWriter()
		) {
			filter.setParameters(cp);
			try {
				filter.open(
					new RawDocument(
						inputPath.toUri(),
						OpenXMLFilter.ENCODING.name(),
						sourceLocale,
						targetLocale
					),
					true
				);
			} catch (Exception e) {
				throw new OkapiException(e);
			}
			writer.setParameters(cp);
			writer.setOptions(targetLocale, OpenXMLFilter.ENCODING.name());
			writer.setOutput(outputPath.toString());
			while (filter.hasNext()) {
				final Event event = filter.next();
				if (event.isTextUnit() && "(Testing symbols)".equals(event.getTextUnit().getSource().getCodedText())) {
					event.getTextUnit().setTarget(
						targetLocale,
						new TextContainer("(واحد)")
					);
				}
				writer.handleEvent(event);
			}
			final OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(
				Files.newInputStream(goldPath),
				Files.newInputStream(outputPath)
			);
			boolean same = differ.isIdentical();
			if (!same) {
				LOGGER.warn("{}{}", inputPath.getFileName(), " FAILED");
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			differ.cleanup();
			assertTrue(same);
		}
	}

	@Test
	public void textRenderingClarifiedForRTLDirectionWithSameLocale() {
		final ConditionalParameters cp = new ConditionalParameters();
		runOneTest("1127-2.pptx", false, false, cp, "", LocaleId.ARABIC, LocaleId.ARABIC);
		assertTrue(allGood);
	}

	@Test
	public void selectivePartsTranslationAndReorderingIntroduced() {
		final ConditionalParameters cp = new ConditionalParameters();
		cp.setTranslateDocProperties(true);
		cp.setReorderPowerpointDocProperties(true);
		runOneTest("1174.pptx", false, false, cp);
		assertTrue(allGood);
	}

	private void runTestTwice (String filename, ConditionalParameters cparams) {
		try {

			Path inputPath = root.in("/" + filename).asPath();
			Path outputPath1 = root.out("/1_" + filename).asPath();

			roundTrip(inputPath, outputPath1, cparams);

			Path outputPath2 = root.out("/2_" + filename).asPath();

			roundTrip(outputPath1, outputPath2, cparams);

			OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(Files.newInputStream(outputPath1),
																   Files.newInputStream(outputPath2));
			boolean same = differ.isIdentical();
			if (!same) {
				LOGGER.warn("{}{}", filename, (same ? " SUCCEEDED" : " FAILED"));
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			differ.cleanup();
			assertTrue(same);
		}
		catch ( Throwable e ) {
			LOGGER.warn("Failed to roundtrip file {}", filename, e);
			fail("An unexpected exception was thrown on file '" + filename+e.getMessage());
		}
	}

	private void roundTrip(Path inputFullPath, Path outputFullPath, ConditionalParameters cparams) throws Exception {
		try (
			final IFilter filter = new OpenXMLFilter();
			final IFilterWriter writer = filter.createFilterWriter()
		) {
			filter.setParameters(cparams);
			try {
				filter.open(new RawDocument(inputFullPath.toUri(),OpenXMLFilter.ENCODING.name(), locENUS),true); // DWH 7-16-09 squishiness
			} catch(Exception e) {
				throw new OkapiException(e);
			}

			writer.setParameters(cparams);
			writer.setOptions(locENUS, OpenXMLFilter.ENCODING.name());
			writer.setOutput(outputFullPath.toString());

			while ( filter.hasNext() ) {
				Event event = filter.next();
				if (event != null) {
					writer.handleEvent(event);
				}
			}
		}
	}

	private ConditionalParameters getParametersFromUserInterface()
	{
		ConditionalParameters parms;
//    Choose the first to get the UI $$$
//		parms = (new Editor()).getParametersFromUI(new ConditionalParameters());
		parms = new ConditionalParameters();
		return parms;
	}
}
