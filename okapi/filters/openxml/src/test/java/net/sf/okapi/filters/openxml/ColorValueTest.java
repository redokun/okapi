/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class ColorValueTest {
    private final XMLFactories xmlfactories;
    private final FileLocation root;
    private final PresetColorValues presetColorValues;

    public ColorValueTest() {
        this.xmlfactories = new XMLFactoriesForTest();
        this.root = FileLocation.fromClass(getClass());
        this.presetColorValues = new PresetColorValues.Default();
    }

    @Test
    public void percentageRgbValueAsRgbRepresented() {
        //  <a:scrgbClr r="50000" g="50000" b="50000"/>
        Assertions.assertThat(new Color.PercentageRgb.Value(presetColorValues, "49600", "49700", "49800").asRgb()).isEqualTo("7E7F7F");
        Assertions.assertThat(new Color.PercentageRgb.Value(this.presetColorValues, "49900", "50000", "50100").asRgb()).isEqualTo("7F8080");
        Assertions.assertThat(new Color.PercentageRgb.Value(this.presetColorValues, "50200", "50300", "50400").asRgb()).isEqualTo("808081");
    }

    @Test
    public void percentageRgbValueAsPercentagesRepresented() {
        //  <a:scrgbClr r="50000" g="50000" b="50000"/>
        Assertions.assertThat(new Color.PercentageRgb.Value(this.presetColorValues, "7D7E7F").asPercentages()).isEqualTo(new double[] {49020.0, 49412.0, 49804.0});
        Assertions.assertThat(new Color.PercentageRgb.Value(this.presetColorValues, "808182").asPercentages()).isEqualTo(new double[] {50196.0, 50588.0, 50980.0});
    }

    @Test
    public void argbAutoValueAsRgbRepresented() {
        final SystemColorValues scv = new SystemColorValues.Default();
        Assertions.assertThat(new Color.Argb.AutoValue(scv, "window").asRgb()).isEqualTo("FFFFFF");
        Assertions.assertThat(new Color.Argb.AutoValue(scv, "windowText").asRgb()).isEqualTo("000000");
    }

    @Test
    public void argbIndexedColorValueAsRgbRepresented() {
        final IndexedColors ic = new IndexedColors.Default(new SystemColorValues.Default());
        Assertions.assertThat(new Color.Argb.IndexedColorValue(this.presetColorValues, ic, "0").asRgb()).isEqualTo("000000");
        Assertions.assertThat(new Color.Argb.IndexedColorValue(this.presetColorValues, ic, "1").asRgb()).isEqualTo("FFFFFF");
        Assertions.assertThat(new Color.Argb.IndexedColorValue(this.presetColorValues, ic, "2").asRgb()).isEqualTo("FF0000");
        Assertions.assertThat(new Color.Argb.IndexedColorValue(this.presetColorValues, ic, "64").asRgb()).isEqualTo("000000");
        Assertions.assertThat(new Color.Argb.IndexedColorValue(this.presetColorValues, ic, "65").asRgb()).isEqualTo("FFFFFF");
    }

    @Test
    public void argbValueAsRgbRepresented() {
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "FFFFFFFF").asRgb()).isEqualTo("FFFFFF");
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "FEFFFFFF").asRgb()).isEqualTo("FEFEFE");
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "80FFFFFF").asRgb()).isEqualTo("808080");
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "7FFFFFFF").asRgb()).isEqualTo("7F7F7F");
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "01FFFFFF").asRgb()).isEqualTo("010101");
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "00FFFFFF").asRgb()).isEqualTo("000000");
    }

    @Test
    public void argbThemeValueAsRgbRepresented() throws IOException, XMLStreamException {
        final Theme theme = new Theme.Default(this.presetColorValues);
        try (final Reader reader = new InputStreamReader(root.in("/xlsx_parts/theme1.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
            theme.readWith(this.xmlfactories.getInputFactory().createXMLEventReader(reader));
        }
        Assertions.assertThat(new Color.Argb.ThemeValue(this.presetColorValues, theme, "0").asRgb()).isEqualTo("000000");
        Assertions.assertThat(new Color.Argb.ThemeValue(this.presetColorValues, theme, "1").asRgb()).isEqualTo("FFFFFF");
        Assertions.assertThat(new Color.Argb.ThemeValue(this.presetColorValues, theme, "2").asRgb()).isEqualTo("1F497D");
        Assertions.assertThat(new Color.Argb.ThemeValue(this.presetColorValues, theme, "10").asRgb()).isEqualTo("0000FF");
    }

    @Test
    public void argbValueWithTintAsRgbRepresented() {
        Assertions.assertThat(new Color.Argb.ValueWithTint(this.presetColorValues, new Color.Argb.Value(this.presetColorValues, "FF808080"), "0").asRgb()).isEqualTo("808080");
        Assertions.assertThat(new Color.Argb.ValueWithTint(this.presetColorValues, new Color.Argb.Value(this.presetColorValues, "FFFFFFFF"), "-1").asRgb()).isEqualTo("000000");
        Assertions.assertThat(new Color.Argb.ValueWithTint(this.presetColorValues, new Color.Argb.Value(this.presetColorValues, "FF000000"), "1").asRgb()).isEqualTo("FFFFFF");
        Assertions.assertThat(new Color.Argb.ValueWithTint(this.presetColorValues, new Color.Argb.Value(this.presetColorValues, "FFFFFFFF"), "-0.5").asRgb()).isEqualTo("808080");
        Assertions.assertThat(new Color.Argb.ValueWithTint(this.presetColorValues, new Color.Argb.Value(this.presetColorValues, "FF000000"), "0.5").asRgb()).isEqualTo("808080");
    }

    @Test
    public void hslValueAsRgbRepresented() {
        // from the spec: The color blue having RGB value RRGGBB = (00, 00, 80) is equivalent to
        // <a:hslClr hue="14400000" sat="100000" lum="50000">
        // However, it looks like the luminance value has to be "25000"...
        Assertions.assertThat(new Color.Hsl.Value(this.presetColorValues, "14400000", "100000", "25000").asRgb()).isEqualTo("000080");
    }

    @Test
    public void argbValueAsHslRepresented() {
        // <a:hslClr hue="14400000" sat="100000" lum="50000">
        Assertions.assertThat(new Color.Argb.Value(this.presetColorValues, "FF000080").asHsl()).isEqualTo(new String[] {"14400000", "100000", "25098"});
    }

    @Test
    public void presetValueAsExternalNameAndRgbRepresented() {
        final QName attributeName = new QName("val");
        final List<Attribute> attributes = Collections.singletonList(
            this.xmlfactories.getEventFactory().createAttribute(attributeName, "aliceBlue")
        );
        final StartElement startElement = this.xmlfactories.getEventFactory().createStartElement(
            Namespace.PREFIX_EMPTY,
            Namespace.EMPTY,
            "test-element",
            attributes.iterator(),
            null
        );
        final Color.Value cv = new Color.Preset(
            this.presetColorValues,
            new Color.Default(this.presetColorValues, startElement)
        ).value();
        Assertions.assertThat(cv.asInternalName()).isEqualTo("aliceBlue");
        Assertions.assertThat(cv.asExternalName()).isEqualTo("Alice Blue");
        Assertions.assertThat(cv.asRgb()).isEqualTo("F0F8FF");
    }
}
