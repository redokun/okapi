package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.FileLocation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.matchers.CompareMatcher;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class WorksheetTest {
	private static final String WORKSHEET_NAME = "test";
	private final XMLFactories xmlfactories = new XMLFactoriesForTest();
	private final FileLocation root;
	private final PresetColorValues presetColorValues;
	private final PresetColorValues highlightColorValues;

	public WorksheetTest() {
		this.root = FileLocation.fromClass(getClass());
		this.presetColorValues = new PresetColorValues.Default();
		this.highlightColorValues = new PresetColorValues.Default(Collections.emptyList());
	}

	@Test
	public void test() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			false,
			new Cells.Default(xmlfactories.getEventFactory(), new ArrayList<>()),
			new StyleDefinitions.Empty(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				true
			)
		);
		read(worksheet, "/xlsx_parts/sheet1.xml");
		StringWriter sw = new StringWriter();
		XMLEventWriter writer = xmlfactories.getOutputFactory().createXMLEventWriter(sw);
		for (final XMLEvent event : worksheet.asMarkup().getEvents()) {
			writer.add(event);
		}
		writer.close();
		assertThat(Input.fromStream(root.in("/xlsx_parts/gold/Rewritten_sheet1.xml").asInputStream()),
				CompareMatcher.isIdenticalTo(sw.toString())
						.withDifferenceEvaluator(DifferenceEvaluators.ignorePrologDifferences()));
	}

	@Test
	public void testExcludeColors() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		conditionalParameters.tsExcelExcludedColors.add("800000");
		conditionalParameters.tsExcelExcludedColors.add("FF0000");
		final Theme theme = new Theme.Default(new PresetColorValues.Default());
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/theme1.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
			theme.readWith(this.xmlfactories.getInputFactory().createXMLEventReader(reader));
		}
		final StyleDefinitions styleDefinitions = new ExcelStyleDefinitions();
		try (final Reader reader = new InputStreamReader(this.root.in("/xlsx_parts/rgb_styles.xml").asInputStream(), OpenXMLFilter.ENCODING)) {
			final SystemColorValues systemColorValues = new SystemColorValues.Default();
			styleDefinitions.readWith(
				new ExcelStyleDefinitionsReader(
					conditionalParameters,
					this.xmlfactories.getEventFactory(),
                    this.presetColorValues,
                    this.highlightColorValues,
					systemColorValues,
					new IndexedColors.Default(systemColorValues),
					theme,
					this.xmlfactories.getInputFactory().createXMLEventReader(reader)
				)
			);
		}
		final Cells cells = new Cells.Default(xmlfactories.getEventFactory(), new ArrayList<>());
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			false,
			cells,
			styleDefinitions,
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/rgb_sheet1.xml");
		int index = 0;
		final Iterator<Cell> iterator = cells.iterator();
		while (iterator.hasNext()) {
			final Cell cell = iterator.next();
			if (0 == index  || 1 == index) {
				assertTrue(cell.excluded());
			} else {
				assertFalse(cell.excluded());
			}
			index++;
		}
	}

	@Test
	public void testExcludeHiddenCells() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final Cells cells = new Cells.Default(xmlfactories.getEventFactory(), new ArrayList<>());
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			false,
			cells,
			new StyleDefinitions.Empty(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/worksheet-hiddenCells.xml");
		int index = 0;
		final Iterator<Cell> iterator = cells.iterator();
		while (iterator.hasNext()) {
			final Cell cell = iterator.next();
			if (0 == index) {
				assertFalse(cell.excluded());
			} else {
				assertTrue(cell.excluded());
			}
			index++;
		}
	}

	@Test
	public void testExposeHiddenCells() throws Exception {
		final ConditionalParameters conditionalParameters = new ConditionalParameters();
		final Cells cells = new Cells.Default(xmlfactories.getEventFactory(), new ArrayList<>());
		final Worksheet worksheet = new Worksheet.Default(
			conditionalParameters,
			xmlfactories.getEventFactory(),
			false,
			cells,
			new StyleDefinitions.Empty(),
			WORKSHEET_NAME,
			new WorksheetFragments.Default(
				conditionalParameters.getTranslateExcelHidden(),
				false
			)
		);
		read(worksheet, "/xlsx_parts/worksheet-hiddenCells.xml");
		int index = 0;
		final Iterator<Cell> iterator = cells.iterator();
		while (iterator.hasNext()) {
			final Cell cell = iterator.next();
			if (0 == index) {
				assertFalse(cell.excluded());
			} else {
				assertTrue(cell.excluded());
			}
			index++;
		}
	}

	private void read(Worksheet worksheet, String resourceName) throws Exception {
		XMLEventReader reader = xmlfactories.getInputFactory().createXMLEventReader(
				root.in(resourceName).asInputStream(), StandardCharsets.UTF_8.name());
		worksheet.readWith(reader);
		reader.close();
	}
}
