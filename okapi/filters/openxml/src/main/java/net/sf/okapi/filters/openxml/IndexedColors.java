/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

interface IndexedColors {
    String NAME = "indexedColors";
    String rgbValueFor(final int index);
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    class Default implements IndexedColors {
        private final SystemColorValues systemColorValues;
        private final List<String> rgbValues;

        Default(final SystemColorValues systemColorValues) {
            this(
                systemColorValues,
                Arrays.asList(
                    "000000", // 0
                    "FFFFFF",
                    "FF0000",
                    "00FF00",

                    "0000FF", // 4
                    "FFFF00",
                    "FF00FF",
                    "00FFFF",

                    "000000", // 8
                    "FFFFFF",
                    "FF0000",
                    "00FF00",

                    "0000FF", // 12
                    "FFFF00",
                    "FF00FF",
                    "00FFFF",

                    "800000", // 16
                    "008000",
                    "000080",
                    "808000",

                    "800080", // 20
                    "008080",
                    "C0C0C0",
                    "808080",

                    "9999FF", // 24
                    "993366",
                    "FFFFCC",
                    "CCFFFF",

                    "660066", // 28
                    "FF8080",
                    "0066CC",
                    "CCCCFF",

                    "000080", // 32
                    "FF00FF",
                    "FFFF00",
                    "00FFFF",

                    "800080", // 36
                    "800000",
                    "008080",
                    "0000FF",

                    "00CCFF", // 40
                    "CCFFFF",
                    "CCFFCC",
                    "FFFF99",

                    "99CCFF", // 44
                    "FF99CC",
                    "CC99FF",
                    "FFCC99",

                    "3366FF", // 48
                    "33CCCC",
                    "99CC00",
                    "FFCC00",

                    "FF9900", // 52
                    "FF6600",
                    "666699",
                    "969696",

                    "003366", // 56
                    "339966",
                    "003300",
                    "333300",

                    "993300", // 60
                    "993366",
                    "333399",
                    "333333"
                )
            );
        }

        Default(final SystemColorValues systemColorValues, final List<String> rgbValues) {
            this.systemColorValues = systemColorValues;
            this.rgbValues = rgbValues;
        }

        @java.lang.Override
        public String rgbValueFor(final int index) {
            final String value;
            if (64 == index) {
                value = this.systemColorValues.valueFor(Color.System.DEFAULT_FOREGROUND_NAME).asRgb();
            } else if (65 == index) {
                value = this.systemColorValues.valueFor(Color.System.DEFAULT_BACKGROUND_NAME).asRgb();
            } else {
                value = this.rgbValues.get(index);
            }
            return value;
        }

        @java.lang.Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @java.lang.Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    class Override implements IndexedColors {
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors.Default defaultIndexColors;
        private final Theme theme;
        private final StartElement startElement;
        private final Map<Integer, Color> colors;
        private EndElement endElement;

        Override(
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final Default defaultIndexColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this(
                eventFactory,
                presetColorValues, systemColorValues, defaultIndexColors,
                theme,
                startElement,
                new LinkedHashMap<>()
            );
        }

        Override(
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final Default defaultIndexColors,
            final Theme theme,
            final StartElement startElement,
            final Map<Integer, Color> colors
        ) {
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.systemColorValues = systemColorValues;
            this.defaultIndexColors = defaultIndexColors;
            this.theme = theme;
            this.startElement = startElement;
            this.colors = colors;
        }

        @java.lang.Override
        public String rgbValueFor(final int index) {
            final String value;
            if (this.colors.containsKey(index)) {
                value = this.colors.get(index).value().asRgb();
            } else {
                value = this.defaultIndexColors.rgbValueFor(index);
            }
            return value;
        }

        @java.lang.Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            int index = 0;
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && Color.Argb.RGB_NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final Color color = new Color.Argb(
                        this.presetColorValues,
                        this.systemColorValues,
                        this,
                        this.theme,
                        new Color.Default(presetColorValues, e.asStartElement())
                    );
                    color.readWith(reader);
                    this.colors.put(index, color);
                    index++;
                }
            }
        }

        @java.lang.Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            this.colors.values().forEach(c -> mb.add(c.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }
}
