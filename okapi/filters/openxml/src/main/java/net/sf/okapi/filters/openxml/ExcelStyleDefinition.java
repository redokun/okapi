/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

interface ExcelStyleDefinition extends StyleDefinition {

    final class Default implements ExcelStyleDefinition {
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private final List<XMLEvent> events;
        private EndElement endElement;

        Default(final XMLEventFactory eventFactory, final StartElement startElement) {
            this(eventFactory, startElement, new LinkedList<>());
        }

        Default(final XMLEventFactory eventFactory, final StartElement startElement, final List<XMLEvent> events) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
            this.events = events;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                this.events.add(e);
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.events.isEmpty() && !this.startElement.getAttributes().hasNext()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            mb.addAll(this.events);
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class NumberFormats implements ExcelStyleDefinition {
        static final String NAME = "numFmts";
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private final Map<Integer, NumberFormat> formats;
        private EndElement endElement;

        NumberFormats(final XMLEventFactory eventFactory, final StartElement startElement) {
            this(eventFactory, startElement, new HashMap<>());
        }

        NumberFormats(
            final XMLEventFactory eventFactory,
            final StartElement startElement,
            final Map<Integer, NumberFormat> formats
        ) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
            this.formats = formats;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && NumberFormat.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final NumberFormat format = new NumberFormat.Explicit(e.asStartElement());
                    format.readWith(reader);
                    this.formats.put(format.id(), format);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        NumberFormat referencedBy(final int id) {
            if (this.formats.containsKey(id)) {
                return this.formats.get(id);
            } else if (NumberFormat.BuiltIn.ids.contains(id)) {
                // todo: provide support for all number formats with date or time values
                //  depending on the source language
                // todo: provide support for other number formats
                return new NumberFormat.BuiltIn(id);
            } else {
                throw new IllegalStateException("The requested number format is not available: ".concat(String.valueOf(id)));
            }
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.formats.isEmpty() || this.formats.entrySet().stream().anyMatch(e -> NumberFormat.BuiltIn.ids.contains(e.getKey()))) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.formats.entrySet().stream()
                .filter(e -> !NumberFormat.BuiltIn.ids.contains(e.getKey()))
                .forEach(fe -> mb.add(fe.getValue().asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class Fonts implements ExcelStyleDefinition {
        static final String NAME = "fonts";
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final PresetColorValues highlightColorValues;
        private final StartElement startElement;
        private final List<Font> fonts;
        private EndElement endElement;

        Fonts(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final StartElement startElement
        ) {
            this(
                conditionalParameters,
                eventFactory,
                presetColorValues,
                highlightColorValues,
                startElement,
                new ArrayList<>()
            );
        }

        Fonts(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues, final StartElement startElement,
            final List<Font> fonts
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.highlightColorValues = highlightColorValues;
            this.startElement = startElement;
            this.fonts = fonts;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && Font.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final Font font = new Font.Default(
                        this.conditionalParameters,
                        this.eventFactory,
                        this.presetColorValues,
                        this.highlightColorValues,
                        e.asStartElement()
                    );
                    font.readWith(reader);
                    this.fonts.add(font);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        Font referencedBy(final int id) {
            if (0 > id || id >= this.fonts.size()) {
                throw new IllegalStateException("The requested font is not available: ".concat(String.valueOf(id)));
            }
            return this.fonts.get(id);
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.fonts.isEmpty()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.fonts.forEach(font -> mb.add(font.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class Fills implements ExcelStyleDefinition {
        static final String NAME = "fills";
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexedColors;
        private final Theme theme;
        private final StartElement startElement;
        private final List<Fill> fills;
        private EndElement endElement;

        Fills(
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this(
                eventFactory,
                presetColorValues,
                systemColorValues,
                indexedColors,
                theme,
                startElement,
                new ArrayList<>()
            );
        }

        Fills(
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final StartElement startElement,
            final List<Fill> fills
        ) {
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.systemColorValues = systemColorValues;
            this.indexedColors = indexedColors;
            this.theme = theme;
            this.startElement = startElement;
            this.fills = fills;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && Fill.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final Fill fill = new Fill.Default(
                        this.presetColorValues,
                        this.systemColorValues,
                        this.indexedColors,
                        this.theme,
                        e.asStartElement()
                    );
                    fill.readWith(reader);
                    this.fills.add(fill);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        Fill referencedBy(final int id) {
            if (0 > id || id >= this.fills.size()) {
                throw new IllegalStateException("The requested fill is not available: ".concat(String.valueOf(id)));
            }
            return this.fills.get(id);
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.fills.isEmpty()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.fills.forEach(fill -> mb.add(fill.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class Borders implements ExcelStyleDefinition {
        static final String NAME = "borders";
        private final ExcelStyleDefinition.Default defaultStyleDefinition;

        Borders(final ExcelStyleDefinition.Default defaultStyleDefinition) {
            this.defaultStyleDefinition = defaultStyleDefinition;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultStyleDefinition.readWith(reader);
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            return this.defaultStyleDefinition.paragraphProperties();
        }

        @Override
        public RunProperties runProperties() {
            return this.defaultStyleDefinition.runProperties();
        }

        @Override
        public Markup asMarkup() {
            return this.defaultStyleDefinition.asMarkup();
        }
    }

    final class CellStyleFormats implements ExcelStyleDefinition {
        static final String NAME = "cellStyleXfs";
        private final ExcelStyleDefinition.CellFormats cellFormats;

        CellStyleFormats(final ExcelStyleDefinition.CellFormats cellFormats) {
            this.cellFormats = cellFormats;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.cellFormats.readWith(reader);
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            return this.cellFormats.paragraphProperties();
        }

        @Override
        public RunProperties runProperties() {
            return this.cellFormats.runProperties();
        }

        @Override
        public Markup asMarkup() {
            return this.cellFormats.asMarkup();
        }
    }

    final class CellFormats implements ExcelStyleDefinition {
        static final String NAME = "cellXfs";
        static final int DEFAULT_INDEX = 0;
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private final List<CellFormat> formats;
        private EndElement endElement;

        CellFormats(final XMLEventFactory eventFactory, final StartElement startElement) {
            this(eventFactory, startElement, new ArrayList<>());
        }

        CellFormats(final XMLEventFactory eventFactory, final StartElement startElement, final List<CellFormat> formats) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
            this.formats = formats;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && CellFormat.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final CellFormat format = new CellFormat.Default(this.eventFactory, e.asStartElement());
                    format.readWith(reader);
                    this.formats.add(format);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        CellFormat referencedBy(final int index) {
            if (0 > index || index >= this.formats.size()) {
                throw new IllegalStateException("The requested format is not available: ".concat(String.valueOf(index)));
            }
            return this.formats.get(index);
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.formats.isEmpty()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.formats.forEach(style -> mb.add(style.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class CellStyles implements ExcelStyleDefinition {
        static final String NAME = "cellStyles";
        private final XMLEventFactory eventFactory;
        private final StartElement startElement;
        private final List<CellStyle> styles;
        private EndElement endElement;

        CellStyles(final XMLEventFactory eventFactory, final StartElement startElement) {
            this(eventFactory, startElement, new ArrayList<>());
        }

        CellStyles(final XMLEventFactory eventFactory, final StartElement startElement, final List<CellStyle> styles) {
            this.eventFactory = eventFactory;
            this.startElement = startElement;
            this.styles = styles;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && CellStyle.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final CellStyle style = new CellStyle.Default(e.asStartElement());
                    style.readWith(reader);
                    this.styles.add(style);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.styles.isEmpty()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.styles.forEach(style -> mb.add(style.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class DifferentialFormats implements ExcelStyleDefinition {
        static final String NAME = "dxfs";
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final PresetColorValues highlightColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexColors;
        private final Theme theme;
        private final StartElement startElement;
        private final List<DifferentialFormat> formats;
        private EndElement endElement;

        DifferentialFormats(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this(
                conditionalParameters,
                eventFactory,
                presetColorValues,
                highlightColorValues,
                systemColorValues,
                indexColors,
                theme,
                startElement,
                new ArrayList<>()
            );
        }

        DifferentialFormats(
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final PresetColorValues highlightColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexColors,
            final Theme theme,
            final StartElement startElement,
            final List<DifferentialFormat> formats
        ) {
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.highlightColorValues = highlightColorValues;
            this.systemColorValues = systemColorValues;
            this.indexColors = indexColors;
            this.theme = theme;
            this.startElement = startElement;
            this.formats = formats;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement() && DifferentialFormat.NAME.equals(e.asStartElement().getName().getLocalPart())) {
                    final DifferentialFormat format = new DifferentialFormat.Default(
                        this.conditionalParameters,
                        this.eventFactory,
                        this.presetColorValues,
                        this.highlightColorValues,
                        this.systemColorValues,
                        this.indexColors,
                        this.theme,
                        e.asStartElement()
                    );
                    format.readWith(reader);
                    this.formats.add(format);
                }
            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup markup = new Markup.General(new LinkedList<>());
            if (this.formats.isEmpty() && !this.startElement.getAttributes().hasNext()) {
                return markup;
            }
            final MarkupBuilder mb = new MarkupBuilder(markup);
            mb.add(this.startElement);
            this.formats.forEach(format -> mb.add(format.asMarkup()));
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class TableStyles implements ExcelStyleDefinition {
        static final String NAME = "tableStyles";
        private final ExcelStyleDefinition.Default defaultStyleDefinition;

        TableStyles(final ExcelStyleDefinition.Default defaultStyleDefinition) {
            this.defaultStyleDefinition = defaultStyleDefinition;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultStyleDefinition.readWith(reader);
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            return this.defaultStyleDefinition.paragraphProperties();
        }

        @Override
        public RunProperties runProperties() {
            return this.defaultStyleDefinition.runProperties();
        }

        @Override
        public Markup asMarkup() {
            return this.defaultStyleDefinition.asMarkup();
        }
    }

    final class Colors implements ExcelStyleDefinition {
        static final String NAME = "colors";
        private static final String MOST_RECENTLY_USED_COLORS = "mruColors";
        private final XMLEventFactory eventFactory;
        private final PresetColorValues presetColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors.Default defaultIndexedColors;
        private final Theme theme;
        private final StartElement startElement;
        private IndexedColors indexedColors;
        private List<XMLEvent> mostRecentlyUsedColorsEvents;
        private EndElement endElement;

        Colors(
            final XMLEventFactory eventFactory,
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors.Default defaultIndexedColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this.eventFactory = eventFactory;
            this.presetColorValues = presetColorValues;
            this.systemColorValues = systemColorValues;
            this.defaultIndexedColors = defaultIndexedColors;
            this.theme = theme;
            this.startElement = startElement;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (IndexedColors.NAME.equals(se.getName().getLocalPart())) {
                    this.indexedColors = new IndexedColors.Override(
                        this.eventFactory,
                        this.presetColorValues,
                        this.systemColorValues,
                        this.defaultIndexedColors,
                        this.theme,
                        e.asStartElement()
                    );
                    this.indexedColors.readWith(reader);
                    continue;
                }
                if (MOST_RECENTLY_USED_COLORS.equals(se.getName().getLocalPart())) {
                    this.mostRecentlyUsedColorsEvents = XMLEventHelpers.eventsFor(se, reader);
                }

            }
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public RunProperties runProperties() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Markup asMarkup() {
            final Markup markup;
            if (null == this.indexedColors && null == this.mostRecentlyUsedColorsEvents) {
                markup = new Markup.Empty();
            } else {
                final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
                mb.add(this.startElement);
                mb.add(indexedColors().asMarkup());
                mb.addAll(mostRecentlyUsedColorsEvents());
                mb.add(endElement());
                markup = mb.build();
            }
            return markup;
        }

        private IndexedColors indexedColors() {
            if (null == this.indexedColors) {
                this.indexedColors = this.defaultIndexedColors;
            }
            return this.indexedColors;
        }

        private List<XMLEvent> mostRecentlyUsedColorsEvents() {
            if (null == this.mostRecentlyUsedColorsEvents) {
                this.mostRecentlyUsedColorsEvents = Collections.emptyList();
            }
            return this.mostRecentlyUsedColorsEvents;
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }

    final class Extensions implements ExcelStyleDefinition {
        static final String NAME = "extLst";
        private final ExcelStyleDefinition.Default defaultStyleDefinition;

        Extensions(final ExcelStyleDefinition.Default defaultStyleDefinition) {
            this.defaultStyleDefinition = defaultStyleDefinition;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultStyleDefinition.readWith(reader);
        }

        @Override
        public ParagraphBlockProperties paragraphProperties() {
            return this.defaultStyleDefinition.paragraphProperties();
        }

        @Override
        public RunProperties runProperties() {
            return this.defaultStyleDefinition.runProperties();
        }

        @Override
        public Markup asMarkup() {
            return this.defaultStyleDefinition.asMarkup();
        }
    }
}
