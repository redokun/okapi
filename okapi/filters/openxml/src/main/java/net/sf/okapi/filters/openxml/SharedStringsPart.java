/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.XMLEventsReader;
import net.sf.okapi.common.annotation.XLIFFContextGroup;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.SubFilter;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartGroup;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

class SharedStringsPart extends StyledTextPart {
    static final String NAME = "sharedStrings.xml";
    private static final String EMPTY = "";
    private static final String ROW_METADATA = "row-metadata";
    private static final String WORKSHEETS = "worksheets";
    private static final String ROWS = "rows";
    private final EncoderManager encoderManager;
    private final IFilter subfilter;
    private final Map<Metadata, Set<String>> dispersedTranslationRawSourcesByMetadata;
    private final Cells cells;
    private IdGenerator worksheetGroupIds;
    private IdGenerator rowGroupIds;
    private StartGroup worksheetStartGroup;
    private StartGroup rowStartGroup;
    private ContextTypes contextTypes;
    private List<XLIFFContextGroup.Context> groupContexts;

    SharedStringsPart(
        final Document.General generalDocument,
        final ZipEntry entry,
        final PresetColorValues highlightColorValues,
        final StyleDefinitions styleDefinitions,
        final StyleOptimisation styleOptimisation,
        final ContentCategoriesDetection contentCategoriesDetection,
        final EncoderManager encoderManager,
        final IFilter subfilter,
        final Map<Metadata, Set<String>> dispersedTranslationRawSourcesByMetadata,
        final Cells cells
    ) {
        super(generalDocument, entry, highlightColorValues, styleDefinitions, styleOptimisation, contentCategoriesDetection);
        this.encoderManager = encoderManager;
        this.subfilter = subfilter;
        this.dispersedTranslationRawSourcesByMetadata = dispersedTranslationRawSourcesByMetadata;
        this.cells = cells;
    }

    /**
     * Opens this part and performs any initial processing.  Returns the
     * first event for this part.  In this case, it's a START_SUBDOCUMENT
     * event.
     *
     * @return Event
     * @throws IOException
     * @throws XMLStreamException
     */
    @Override
    public Event open() throws IOException, XMLStreamException {
        this.documentId = this.generalDocument.documentId();
        this.subDocumentId = this.generalDocument.nextSubDocumentId();
        this.sourceLocale = this.generalDocument.sourceLocale();
        this.worksheetGroupIds = new IdGenerator(WORKSHEETS, IdGenerator.START_GROUP);
        final SharedStrings sharedStrings = new SharedStrings.Default(
            this.generalDocument.conditionalParameters(),
            this.generalDocument.eventFactory(),
            this.generalDocument.presetColorValues(),
            this.highlightColorValues,
            this.nestedBlockId,
            this.styleDefinitions,
            this.styleOptimisation,
            this.contentCategoriesDetection
        );
        if (null == this.generalDocument.zipFile().getEntry(this.entry.getName())) {
            sharedStrings.readWith(new XMLEventsReader(emptyPartEvents()));
        } else {
            try (final Reader reader = this.generalDocument.getPartReader(this.entry.getName())) {
                sharedStrings.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
            }
        }
        sharedStrings.addFrom(this.cells.of(CellType.INLINE_STRING));
        formEventsFor(sharedStrings);
        return createStartSubDocumentEvent(documentId, subDocumentId);
    }

    private List<XMLEvent> emptyPartEvents() throws XMLStreamException, IOException {
        final Iterator<Namespace> iterator = this.generalDocument.mainPartNamespaces().iterator();
        final String prefix;
        final String namespaceUri;
        if (iterator.hasNext()) {
            final Namespace n = iterator.next();
            prefix = n.prefix();
            namespaceUri = n.uri();
        } else {
            prefix = Namespace.PREFIX_EMPTY;
            namespaceUri = Namespace.EMPTY;
        }
        return Arrays.asList(
            this.generalDocument.eventFactory().createStartDocument(StyledTextSkeletonWriter.XML_VERSION, StyledTextSkeletonWriter.XML_ENCODING),
            this.generalDocument.eventFactory().createStartElement(prefix, namespaceUri, SharedStrings.SST),
            this.generalDocument.eventFactory().createEndElement(prefix, namespaceUri, SharedStrings.SST),
            this.generalDocument.eventFactory().createEndDocument()
        );
    }

    private void formEventsFor(final SharedStrings sharedStrings) {
        addEventToDocumentPart(sharedStrings.startDocument());
        addEventToDocumentPart(sharedStrings.startElement());
        flushDocumentPart();
        final ListIterator<Cell> iterator = this.cells.iterator();
        while (iterator.hasNext()) {
            final Cell cell = iterator.next();
            if (this.cells.worksheetStartsAt(iterator)) {
                endRowGroup();
                endWorksheetGroup();
                initRowGroupIdsFrom(cell.worksheetName());
                startWorksheetGroupWith(cell.worksheetName());
                startRowGroupWith(String.valueOf(cell.cellReferencesRange().first().row()));
            } else if (this.cells.rowStartsAt(iterator)) {
                endRowGroup();
                if (!rowGroupIdsInitialised()) {
                    initRowGroupIdsFrom(cell.worksheetName());
                }
                startRowGroupWith(String.valueOf(cell.cellReferencesRange().first().row()));
            }
            if (CellType.INLINE_STRING != cell.type() && CellType.SHARED_STRING != cell.type()) {
                if (cell.valuePresent()) {
                    if (MetadataContext.ROW_AND_COLUMN == cell.metadataContext()) {
                        addContextType(cell.cellReferencesRange(), cell.value().asFormattedString());
                    } else if (MetadataContext.COLUMN == cell.metadataContext()) {
                        addGroupContext(cell.cellReferencesRange(), cell.value().asFormattedString());
                    }
                }
                continue;
            }
            final StringItem stringItem = sharedStrings.stringItemFor(cell.value().asFormerInteger());
            if (MetadataContext.ROW_AND_COLUMN == cell.metadataContext()) {
                addContextType(cell.cellReferencesRange(), stringItem);
            } else if (MetadataContext.COLUMN == cell.metadataContext()) {
                addGroupContext(cell.cellReferencesRange(), stringItem);
            }
            if (cell.excluded()) {
                addBlockChunksToDocumentPart(stringItem.getChunks());
                flushDocumentPart();
                continue;
            }
            final CrossSheetCellReference crossSheetCellReference = new CrossSheetCellReference(
                cell.worksheetName(),
                cell.cellReferencesRange().first()
            );
            final List<ITextUnit> textUnits = new StringItemTextUnitMapper(
                this.textUnitIds,
                this.generalDocument.eventFactory(),
                stringItem,
                crossSheetCellReference
            ).map();
            if (textUnits.isEmpty()) {
                addBlockChunksToDocumentPart(stringItem.getChunks());
                flushDocumentPart();
            } else {
                if (this.subfilter != null && !stringItem.isStyled()) {
                    addSubfilteredEvents(textUnits);
                } else {
                    addTextUnitEvents(textUnits);
                }
                final String rawSource = stringItem.text();
                this.dispersedTranslationRawSourcesByMetadata.entrySet().stream()
                    .filter(
                        e -> e.getKey().worksheetLocalisedName().equals(cell.worksheetName())
                        && e.getKey().cellReferencesRange().anyMatch(cell.cellReferencesRange())
                        && e.getValue().contains(rawSource)
                    )
                    .forEach(e -> this.generalDocument.dispersedTranslations().add(
                        crossSheetCellReference,
                        e.getKey().name(),
                        rawSource
                    ));
            }
        }
        endRowGroup();
        endWorksheetGroup();
        addEventToDocumentPart(sharedStrings.endElement());
        addEventToDocumentPart(sharedStrings.endDocument());
        flushDocumentPart();
        filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(subDocumentId)));
        filterEventIterator = filterEvents.iterator();
    }

    private boolean rowGroupIdsInitialised() {
        return null != this.rowGroupIds;
    }

    private void initRowGroupIdsFrom(final String worksheetName) {
        this.rowGroupIds = new IdGenerator(worksheetName.concat(ROWS), IdGenerator.START_GROUP);
    }

    private boolean worksheetGroupInitialised() {
        return null != this.worksheetStartGroup;
    }

    private void startWorksheetGroupWith(final String name) {
        this.contextTypes = new ContextTypes(
            this.generalDocument.conditionalParameters().worksheetConfigurations().metadataColumnsFor(
                name
            )
        );
        this.worksheetStartGroup = new StartGroup(EMPTY, this.worksheetGroupIds.createId());
        this.worksheetStartGroup.setName(name);
        this.filterEvents.add(
            new Event(
                EventType.START_GROUP,
                this.worksheetStartGroup
            )
        );
    }

    private void endWorksheetGroup() {
        if (worksheetGroupInitialised()) {
            if (this.filterEvents.get(this.filterEvents.size() - 1).isStartGroup()) {
                // do not keep empty groups
                this.filterEvents.remove(this.filterEvents.size() - 1);
            } else {
                this.filterEvents.add(
                    new Event(
                        EventType.END_GROUP,
                        new Ending(this.worksheetStartGroup.getId())
                    )
                );
            }
            this.worksheetStartGroup = null;
        }
    }

    boolean rowGroupInitialised() {
        return null != this.rowStartGroup;
    }

    private void startRowGroupWith(final String name) {
        this.groupContexts = new LinkedList<>();
        this.rowStartGroup = new StartGroup(EMPTY, this.rowGroupIds.createId());
        this.rowStartGroup.setName(String.valueOf(name));
        this.filterEvents.add(
            new Event(
                EventType.START_GROUP,
                this.rowStartGroup
            )
        );
    }

    private void endRowGroup() {
        if (rowGroupInitialised()) {
            if (this.filterEvents.get(this.filterEvents.size() - 1).isStartGroup()) {
                // do not keep empty groups
                this.filterEvents.remove(this.filterEvents.size() - 1);
            } else {
                if (!this.groupContexts.isEmpty()) {
                    this.rowStartGroup.setAnnotation(
                        new XLIFFContextGroup(
                            this.encoderManager,
                            ROW_METADATA,
                            null,
                            null,
                            this.groupContexts
                        )
                    );
                }
                this.filterEvents.add(
                    new Event(
                        EventType.END_GROUP,
                        new Ending(this.rowStartGroup.getId())
                    )
                );
            }
            this.rowStartGroup = null;
        }
    }

    private void addContextType(final CellReferencesRange cellReferencesRange, final StringItem stringItem) {
        addContextType(cellReferencesRange, stringItem.text());
    }

    private void addContextType(final CellReferencesRange cellReferencesRange, final String string) {
        this.contextTypes.add(cellReferencesRange, string);
    }

    private void addGroupContext(final CellReferencesRange cellReferencesRange, final StringItem stringItem) {
        addGroupContext(cellReferencesRange, stringItem.text());
    }

    private void addGroupContext(final CellReferencesRange cellReferencesRange, final String string) {
        final XLIFFContextGroup.Context context = new XLIFFContextGroup.Context(
            this.encoderManager,
            this.contextTypes.valueFor(cellReferencesRange),
            null,
            null
        );
        context.value(string);
        this.groupContexts.add(context);
    }

    private void addTextUnitEvents(final List<ITextUnit> textUnits) {
        for (final ITextUnit tu : textUnits) {
            filterEvents.add(new Event(EventType.TEXT_UNIT, tu));
        }
    }

    private void addSubfilteredEvents(final List<ITextUnit> textUnits) {
        int subfilterIndex = 0;
        for (final ITextUnit tu : textUnits) {
            try (final SubFilter sf = new SubFilter(subfilter, encoderManager.getEncoder(), ++subfilterIndex, tu.getId(), tu.getName())) {
                filterEvents.addAll(sf.getEvents(new RawDocument(tu.getSource().getFirstContent().getText(), sourceLocale)));
                filterEvents.add(sf.createRefEvent(tu));
            }
        }
    }

    private static class ContextTypes {
        private static final String DELIMITER = ";";
        private final Set<String> metadataColumns;
        private final Map<CellReferencesRange, String> values;

        private ContextTypes(final Set<String> metadataColumns) {
            this(
                metadataColumns,
                new LinkedHashMap<>()
            );
        }

        private ContextTypes(
            final Set<String> metadataColumns,
            final Map<CellReferencesRange, String> values
        ) {
            this.metadataColumns = metadataColumns;
            this.values = values;
        }

        void add(final CellReferencesRange cellReferencesRange, final String value) {
            this.values.put(cellReferencesRange, value);
        }

        String valueFor(final CellReferencesRange cellReferencesRange) {
            final String value = this.values.entrySet().stream()
                .filter(e -> e.getKey().anyMatch(Collections.emptySet(), cellReferencesRange.columns()))
                .map(e -> e.getValue())
                .collect(Collectors.joining(DELIMITER));
            return value.isEmpty()
                ? cellReferencesRange.columnMatches(this.metadataColumns).stream()
                    .collect(Collectors.joining(DELIMITER))
                : value;
        }
    }
}
