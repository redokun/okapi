package net.sf.okapi.filters.openxml;

import java.util.zip.ZipEntry;

import javax.xml.stream.events.XMLEvent;

class ExcelCommentPart extends StyledTextPart {
	ExcelCommentPart(
		final Document.General generalDocument,
		final ZipEntry entry,
		final PresetColorValues highlightColorValues,
		final StyleDefinitions styleDefinitions,
		final StyleOptimisation styleOptimisation,
		final ContentCategoriesDetection contentCategoriesDetection
	) {
		super(generalDocument, entry, highlightColorValues, styleDefinitions, styleOptimisation, contentCategoriesDetection);
	}

	@Override
	protected boolean isStyledBlockStartEvent(XMLEvent e) {
		return XMLEventHelpers.isStartElement(e, "text");
	}
}
