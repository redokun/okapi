/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface PresetColorValues {
    List<String> externalNames();
    Color.Value valueFor(final String value);

    final class Default implements PresetColorValues {
        private final List<Color.Value> values;

        public Default() {
            this(Arrays.asList(
                new Color.Default.Value("aliceBlue", "Alice Blue", 240, 248, 255),
                new Color.Default.Value("antiqueWhite", "Antique White", 250, 235, 215),
                new Color.Default.Value("azure", "Azure", 240, 255, 255),
                new Color.Default.Value("beige", "Beige", 245, 245, 220),
                new Color.Default.Value("bisque", "Bisque", 255, 228, 196),
                new Color.Default.Value("black", "Black", 0, 0, 0),
                new Color.Default.Value("blanchedAlmond", "Blanched Almond", 255, 235, 205),
                new Color.Default.Value("blue", "Blue", 0, 0, 255),
                new Color.Default.Value("blueViolet", "Blue Violet", 138, 43, 226),
                new Color.Default.Value("brown", "Brown", 165, 42, 42),
                new Color.Default.Value("burlyWood", "Burly Wood", 222, 184, 135),
                new Color.Default.Value("cadetBlue", "Cadet Blue", 95, 158, 160),
                new Color.Default.Value("chartreuse", "Chartreuse", 127, 255, 0),
                new Color.Default.Value("chocolate", "Chocolate", 210, 105, 30),
                new Color.Default.Value("coral", "Coral", 255, 127, 80),
                new Color.Default.Value("cornflowerBlue", "Cornflower Blue", 100, 149, 237),
                new Color.Default.Value("cornsilk", "Cornsilk", 255, 248, 220),
                new Color.Default.Value("crimson", "Crimson", 220, 20, 60),
                new Color.Default.Value("cyan", "Cyan", 0, 255, 255),
                new Color.Default.Value("dkBlue", "Dark Blue", 0, 0, 139),
                new Color.Default.Value("dkCyan", "Dark Cyan", 0, 139, 139),
                new Color.Default.Value("dkGoldenrod", "Dark Goldenrod", 184, 134, 11),
                new Color.Default.Value("dkGray", "Dark Gray", 169, 169, 169),
                new Color.Default.Value("dkGreen", "Dark Green", 0, 100, 0),
                new Color.Default.Value("dkKhaki", "Dark Khaki", 189, 183, 107),
                new Color.Default.Value("dkMagenta", "Dark Magenta", 139, 0, 139),
                new Color.Default.Value("dkOliveGreen", "Dark Olive Green", 85, 107, 47),
                new Color.Default.Value("dkOrange", "Dark Orange", 255, 140, 0),
                new Color.Default.Value("dkOrchid", "Dark Orchid", 153, 50, 204),
                new Color.Default.Value("dkRed", "Dark Red", 139, 0, 0),
                new Color.Default.Value("dkSalmon", "Dark Salmon", 233, 150, 122),
                new Color.Default.Value("dkSeaGreen", "Dark Sea Green", 143, 188, 139),
                new Color.Default.Value("dkSlateBlue", "Dark Slate Blue", 72, 61, 139),
                new Color.Default.Value("dkSlateGray", "Dark Slate Gray", 47, 79, 79),
                new Color.Default.Value("dkTurquoise", "Dark Turquoise", 0, 206, 209),
                new Color.Default.Value("dkViolet", "Dark Violet", 148, 0, 211),
                new Color.Default.Value("deepPink", "Deep Pink", 255, 20, 147),
                new Color.Default.Value("deepSkyBlue", "Deep Sky Blue", 0, 191, 255),
                new Color.Default.Value("dimGray", "Dim Gray", 105, 105, 105),
                new Color.Default.Value("dodgerBlue", "Dodger Blue", 30, 144, 255),
                new Color.Default.Value("firebrick", "Firebrick", 178, 34, 34),
                new Color.Default.Value("floralWhite", "Floral White", 255, 250, 240),
                new Color.Default.Value("forestGreen", "Forest Green", 34, 139, 34),
                new Color.Default.Value("fuchsia", "Fuchsia", 255, 0, 255),
                new Color.Default.Value("gainsboro", "Gainsboro", 220, 220, 220),
                new Color.Default.Value("ghostWhite", "Ghost White", 248, 248, 255),
                new Color.Default.Value("gold", "Gold", 255, 215, 0),
                new Color.Default.Value("goldenrod", "Goldenrod", 218, 165, 32),
                new Color.Default.Value("gray", "Gray", 128, 128, 128),
                new Color.Default.Value("green", "Green", 0, 128, 0),
                new Color.Default.Value("greenYellow", "Green Yellow", 173, 255, 47),
                new Color.Default.Value("honeydew", "Honeydew", 240, 255, 240),
                new Color.Default.Value("hotPink", "Hot Pink", 255, 105, 180),
                new Color.Default.Value("indianRed", "Indian Red", 205, 92, 92),
                new Color.Default.Value("indigo", "Indigo", 75, 0, 130),
                new Color.Default.Value("ivory", "Ivory", 255, 255, 240),
                new Color.Default.Value("khaki", "Khaki", 240, 230, 140),
                new Color.Default.Value("lavender", "Lavender", 230, 230, 250),
                new Color.Default.Value("lavenderBlush", "Lavender Blush", 255, 240, 245),
                new Color.Default.Value("lawnGreen", "Lawn Green", 124, 252, 0),
                new Color.Default.Value("lemonChiffon", "Lemon Chiffon", 255, 250, 205),
                new Color.Default.Value("ltBlue", "Light Blue", 173, 216, 230),
                new Color.Default.Value("ltCoral", "Light Coral", 240, 128, 128),
                new Color.Default.Value("ltCyan", "Light Cyan", 224, 255, 255),
                new Color.Default.Value("ltGoldenrodYellow", "Light Goldenrod Yellow", 250, 250, 120),
                new Color.Default.Value("ltGray", "Light Gray", 211, 211, 211),
                new Color.Default.Value("ltGreen", "Light Green", 144, 238, 144),
                new Color.Default.Value("ltPink", "Light Pink", 255, 182, 193),
                new Color.Default.Value("ltSalmon", "Light Salmon", 255, 160, 122),
                new Color.Default.Value("ltSeaGreen", "Light Sea Green", 32, 178, 170),
                new Color.Default.Value("ltSkyBlue", "Light Sky Blue", 135, 206, 250),
                new Color.Default.Value("ltSlateGray", "Light Slate Gray", 119, 136, 153),
                new Color.Default.Value("ltSteelBlue", "Light Steel Blue", 176, 196, 222),
                new Color.Default.Value("ltYellow", "Light Yellow", 255, 255, 224),
                new Color.Default.Value("lime", "Lime", 0, 255, 0),
                new Color.Default.Value("limeGreen", "Lime Green", 50, 205, 50),
                new Color.Default.Value("linen", "Linen", 250, 240, 230),
                new Color.Default.Value("magenta", "Magenta", 255, 0, 255),
                new Color.Default.Value("maroon", "Maroon", 128, 0, 0),
                new Color.Default.Value("medAquamarine", "Medium Aquamarine", 102, 205, 170),
                new Color.Default.Value("medBlue", "Medium Blue", 0, 0, 205),
                new Color.Default.Value("medOrchid", "Medium Orchid", 186, 85, 211),
                new Color.Default.Value("medPurple", "Medium Purple", 147, 112, 219),
                new Color.Default.Value("medSeaGreen", "Medium Sea Green", 60, 179, 113),
                new Color.Default.Value("medSlateBlue", "Medium Slate Blue", 123, 104, 238),
                new Color.Default.Value("medSpringGreen", "Medium Spring Green", 0, 250, 154),
                new Color.Default.Value("medTurquoise", "Medium Turquoise", 72, 209, 204),
                new Color.Default.Value("medVioletRed", "Medium Violet Red", 199, 21, 133),
                new Color.Default.Value("midnightBlue", "Midnight Blue", 25, 25, 112),
                new Color.Default.Value("mintCream", "Mint Cream", 245, 255, 250),
                new Color.Default.Value("mistyRose", "Misty Rose", 255, 228, 225),
                new Color.Default.Value("moccasin", "Moccasin", 255, 228, 181),
                new Color.Default.Value("navajoWhite", "Navajo White", 255, 222, 173),
                new Color.Default.Value("navy", "Navy", 0, 0, 128),
                new Color.Default.Value("oldLace", "Old Lace", 253, 245, 230),
                new Color.Default.Value("olive", "Olive", 128, 128, 0),
                new Color.Default.Value("oliveDrab", "Olive Drab", 107, 142, 35),
                new Color.Default.Value("orange", "Orange", 255, 165, 0),
                new Color.Default.Value("orangeRed", "Orange Red", 255, 69, 0),
                new Color.Default.Value("orchid", "Orchid", 218, 112, 214),
                new Color.Default.Value("paleGoldenrod", "Pale Goldenrod", 238, 232, 170),
                new Color.Default.Value("paleGreen", "Pale Green", 152, 251, 152),
                new Color.Default.Value("paleTurquoise", "Pale Turquoise", 175, 238, 238),
                new Color.Default.Value("paleVioletRed", "Pale Violet Red", 219, 112, 147),
                new Color.Default.Value("papayaWhip", "Papaya Whip", 255, 239, 213),
                new Color.Default.Value("peachPuff", "Peach Puff", 255, 218, 185),
                new Color.Default.Value("peru", "Peru", 205, 133, 63),
                new Color.Default.Value("pink", "Pink", 255, 192, 203),
                new Color.Default.Value("plum", "Plum", 221, 160, 221),
                new Color.Default.Value("powderBlue", "Powder Blue", 176, 224, 230),
                new Color.Default.Value("purple", "Purple", 128, 0, 128),
                new Color.Default.Value("red", "Red", 255, 0, 0),
                new Color.Default.Value("rosyBrown", "Rosy Brown", 188, 143, 143),
                new Color.Default.Value("royalBlue", "Royal Blue", 65, 105, 225),
                new Color.Default.Value("saddleBrown", "Saddle Brown", 139, 69, 19),
                new Color.Default.Value("salmon", "Salmon", 250, 128, 114),
                new Color.Default.Value("sandyBrown", "Sandy Brown", 244, 164, 96),
                new Color.Default.Value("seaGreen", "Sea Green", 46, 139, 87),
                new Color.Default.Value("seaShell", "Sea Shell", 255, 245, 238),
                new Color.Default.Value("sienna", "Sienna", 160, 82, 45),
                new Color.Default.Value("silver", "Silver", 192, 192, 192),
                new Color.Default.Value("skyBlue", "Sky Blue", 135, 206, 235),
                new Color.Default.Value("slateBlue", "Slate Blue", 106, 90, 205),
                new Color.Default.Value("slateGray", "Slate Gray", 112, 128, 144),
                new Color.Default.Value("snow", "Snow", 255, 250, 250),
                new Color.Default.Value("springGreen", "Spring Green", 0, 255, 127),
                new Color.Default.Value("steelBlue", "Steel Blue", 70, 130, 180),
                new Color.Default.Value("tan", "Tan", 210, 180, 140),
                new Color.Default.Value("teal", "Teal", 0, 128, 128),
                new Color.Default.Value("thistle", "Thistle", 216, 191, 216),
                new Color.Default.Value("tomato", "Tomato", 255, 99, 71),
                new Color.Default.Value("turquoise", "Turquoise", 64, 224, 208),
                new Color.Default.Value("violet", "Violet", 238, 130, 238),
                new Color.Default.Value("wheat", "Wheat", 245, 222, 179),
                new Color.Default.Value("white", "White", 255, 255, 255),
                new Color.Default.Value("whiteSmoke", "WhiteSmoke", 245, 245, 245),
                new Color.Default.Value("yellow", "Yellow", 255, 255, 0),
                new Color.Default.Value("yellowGreen", "Yellow Green", 154, 205, 50)
            ));
        }

        Default(final List<Color.Value> values) {
            this.values = values;
        }

        @Override
        public List<String> externalNames() {
            return this.values.stream()
                .map(v -> v.asExternalName())
                .collect(Collectors.toList());
        }

        @Override
        public Color.Value valueFor(final String string) {
            return this.values.stream()
                .filter(v -> v.asRgb().equals(string) || v.asExternalName().equalsIgnoreCase(string) || v.asInternalName().equals(string))
                .findFirst()
                .orElse(new Color.Value.Empty());
        }
    }
}
