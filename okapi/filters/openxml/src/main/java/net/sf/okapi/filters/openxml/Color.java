/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Color {
    Value value();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements Color {
        private Color.Value value;

        @Override
        public Value value() {
            if (null == this.value) {
                this.value = new Color.Value.Empty();
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements Color {
        static final String NAME = "srgbClr";
        private static final String HEX_COLOR_REGEX = "([\\dA-Fa-f]{2})";
        private static final Pattern rgbPattern = Pattern.compile(
            "^".concat(HEX_COLOR_REGEX).concat(HEX_COLOR_REGEX).concat(HEX_COLOR_REGEX).concat("$")
        );
        private static final String EMPTY = "";
        private final PresetColorValues presetColorValues;
        private final StartElement startElement;
        private final QName valueAttributeName;
        private final List<XMLEvent> innerEvents;
        private EndElement endElement;
        private Color.Value value;

        Default(final PresetColorValues presetColorValues, final StartElement startElement) {
            this(presetColorValues, startElement, new QName("val"));
        }

        Default(final PresetColorValues presetColorValues, final StartElement startElement, final QName valueAttributeName) {
            this(presetColorValues, startElement, valueAttributeName, new LinkedList<>());
        }

        Default(
            final PresetColorValues presetColorValues,
            final StartElement startElement,
            final QName valueAttributeName,
            final List<XMLEvent> innerEvents
        ) {
            this.presetColorValues = presetColorValues;
            this.startElement = startElement;
            this.valueAttributeName = valueAttributeName;
            this.innerEvents = innerEvents;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                final String sv = this.startElement.getAttributeByName(this.valueAttributeName).getValue();
                final Color.Value cv = this.presetColorValues.valueFor(sv);
                if (cv.asInternalName().isEmpty() && cv.asExternalName().isEmpty() && cv.asRgb().isEmpty()) {
                    final Matcher m = Color.Default.rgbPattern.matcher(sv);
                    if (m.matches()) {
                        this.value = new Value(
                            Color.Default.EMPTY,
                            Color.Default.EMPTY,
                            sv
                        );
                    } else {
                        this.value = new Value(
                            sv,
                            Color.Default.EMPTY,
                            Color.Default.EMPTY
                        );
                    }
                } else {
                    this.value = cv;
                }
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                this.innerEvents.add(e);
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            mb.addAll(this.innerEvents);
            mb.add(this.endElement);
            return mb.build();
        }

        static final class Value implements Color.Value {
            private final String internalName;
            private final String externalName;
            private final String rgb;

            Value(
                final String internalName,
                final String externalName,
                final int red,
                final int green,
                final int blue
            ) {
                this(internalName, externalName, String.format("%06X", red << 16 | green << 8 | blue));
            }

            Value(final String internalName, final String externalName, final String rgb) {
                this.internalName = internalName;
                this.externalName = externalName;
                this.rgb = rgb;
            }

            @Override
            public boolean matches(final String value) {
                return this.rgb.equals(value)
                    || this.externalName.equalsIgnoreCase(value)
                    || this.internalName.equals(value);
            }

            @Override
            public String asInternalName() {
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                return this.externalName;
            }

            @Override
            public String asRgb() {
                return this.rgb;
            }
        }
    }

    final class PercentageRgb implements Color {
        static final String NAME = "scrgbClr";
        private static final String R = "r";
        private static final String G = "g";
        private static final String B = "b";
        private final PresetColorValues presetColorValues;
        private final Default defaultColor;
        private Color.Value value;

        PercentageRgb(final PresetColorValues presetColorValues, final Default defaultColor) {
            this.presetColorValues = presetColorValues;
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                final Iterator iterator = this.defaultColor.startElement.getAttributes();
                String r = "";
                String g = "";
                String b = "";
                while (iterator.hasNext()) {
                    final Attribute a = (Attribute) iterator.next();
                    switch (a.getName().getLocalPart()) {
                        case R:
                            r = a.getValue();
                            break;
                        case G:
                            g = a.getValue();
                            break;
                        case B:
                            b = a.getValue();
                            break;
                        default:
                            throw new IllegalStateException("Unsupported attribute name: ".concat(a.getName().getLocalPart()));
                    }
                }
                this.value = new Value(this.presetColorValues, r, g, b);
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }

        static class Value implements Color.Value {
            private static final int SCALE = 100_000;
            private static final int MAX_PER_CHANNEL = 255;
            private static final int RADIX = 16;
            private final PresetColorValues presetColorValues;
            private final double redPercentage;
            private final double greenPercentage;
            private final double bluePercentage;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            Value(final PresetColorValues presetColorValues, final String rgb) {
                this(
                    presetColorValues, (double) Math.round(
                        (double) Integer.parseInt(rgb.substring(0, 2), PercentageRgb.Value.RADIX)
                            / PercentageRgb.Value.MAX_PER_CHANNEL * PercentageRgb.Value.SCALE
                    ) /  PercentageRgb.Value.SCALE,
                    (double) Math.round(
                        (double) Integer.parseInt(rgb.substring(2, 4), PercentageRgb.Value.RADIX)
                            / PercentageRgb.Value.MAX_PER_CHANNEL * PercentageRgb.Value.SCALE
                    ) /  PercentageRgb.Value.SCALE,
                    (double) Math.round(
                        (double) Integer.parseInt(rgb.substring(4, 6), PercentageRgb.Value.RADIX)
                            / PercentageRgb.Value.MAX_PER_CHANNEL * PercentageRgb.Value.SCALE
                    ) /  PercentageRgb.Value.SCALE
                );
            }

            Value(final PresetColorValues presetColorValues, final String redPercentage, final String greenPercentage, final String bluePercentage) {
                this(
                    presetColorValues,
                    Double.parseDouble(redPercentage) / PercentageRgb.Value.SCALE,
                    Double.parseDouble(greenPercentage) / PercentageRgb.Value.SCALE,
                    Double.parseDouble(bluePercentage) / PercentageRgb.Value.SCALE
                );
            }

            Value(final PresetColorValues presetColorValues, final double redPercentage, final double greenPercentage, final double bluePercentage) {
                this.presetColorValues = presetColorValues;
                this.redPercentage = redPercentage;
                this.greenPercentage = greenPercentage;
                this.bluePercentage = bluePercentage;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    this.rgb = String.format(
                        "%02X%02X%02X",
                        Math.round(MAX_PER_CHANNEL * this.redPercentage),
                        Math.round(MAX_PER_CHANNEL * this.greenPercentage),
                        Math.round(MAX_PER_CHANNEL * this.bluePercentage)
                    );
                }
                return this.rgb;
            }

            double[] asPercentages() {
                return new double[] {
                    Math.round(this.redPercentage * PercentageRgb.Value.SCALE),
                    Math.round(this.greenPercentage * PercentageRgb.Value.SCALE),
                    Math.round(this.bluePercentage * PercentageRgb.Value.SCALE)
                };
            }
        }
    }

    final class Argb implements Color {
        static final String RGB_NAME = "rgbColor";
        static final String BACKGROUND_NAME = "bgColor";
        static final String FOREGROUND_NAME = "fgColor";
        private static final String AUTO = "auto";
        private static final String INDEXED = "indexed";
        private static final String RGB = "rgb";
        private static final String THEME = "theme";
        private static final String TINT = "tint";
        private final PresetColorValues presetColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexedColors;
        private final Theme theme;
        private final Default defaultColor;
        private Color.Value value;

        Argb(
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final Default defaultColor
        ) {
            this.presetColorValues = presetColorValues;
            this.systemColorValues = systemColorValues;
            this.indexedColors = indexedColors;
            this.theme = theme;
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                String tint = "";
                final Iterator iterator = this.defaultColor.startElement.getAttributes();
                while (iterator.hasNext()) {
                    final Attribute a = (Attribute) iterator.next();
                    switch (a.getName().getLocalPart()) {
                        case AUTO:
                            this.value = new AutoValue(
                                this.systemColorValues,
                                this.defaultColor.startElement.getName().getLocalPart()
                            );
                        case INDEXED:
                            this.value = new IndexedColorValue(this.presetColorValues, this.indexedColors, a.getValue());
                            break;
                        case RGB:
                            this.value = new Value(this.presetColorValues, a.getValue());
                            break;
                        case THEME:
                            this.value = new ThemeValue(this.presetColorValues, this.theme, a.getValue());
                            break;
                        case TINT:
                            tint = a.getValue();
                            break;
                        default:
                            throw new IllegalStateException("Unsupported attribute name: ".concat(a.getName().getLocalPart()));
                    }
                }
                if (!tint.isEmpty()) {
                    // the value has to be transformed to RGB already
                    this.value = new ValueWithTint(this.presetColorValues, (Value) this.value, tint);
                }
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }

        static final class AutoValue implements Color.Value {
            private static final String EMPTY = "";
            private final SystemColorValues systemColorValues;
            private final String internalName;
            private Color.Value systemColorValue;
            private String rgb;

            AutoValue(final SystemColorValues systemColorValues, final String internalName) {
                this.systemColorValues = systemColorValues;
                this.internalName = internalName;
            }

            @Override
            public boolean matches(final String value) {
                final Color.Value cv = systemColorValueFor(this.internalName);
                return cv.asRgb().equals(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value systemColorValueFor(final String internalName) {
                if (null == this.systemColorValue) {
                    this.systemColorValue = this.systemColorValues.valueFor(internalName);
                }
                return this.systemColorValue;
            }

            @Override
            public String asInternalName() {
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                return EMPTY;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    this.rgb = systemColorValueFor(this.internalName).asRgb();
                }
                return this.rgb;
            }
        }

        static final class IndexedColorValue implements Color.Value {
            private final PresetColorValues presetColorValues;
            private final IndexedColors indexedColors;
            private final int index;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            IndexedColorValue(final PresetColorValues presetColorValues, final IndexedColors indexedColors, final String index) {
                this(presetColorValues, indexedColors, Integer.parseUnsignedInt(index));
            }

            IndexedColorValue(final PresetColorValues presetColorValues, final IndexedColors indexedColors, final int index) {
                this.presetColorValues = presetColorValues;
                this.indexedColors = indexedColors;
                this.index = index;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    final String rgb = asRgb();
                    final Color.Value cv = presetColorValueFor(rgb);
                    this.internalName = cv.asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    final String rgb = asRgb();
                    final Color.Value cv = presetColorValueFor(rgb);
                    this.externalName = cv.asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    this.rgb = this.indexedColors.rgbValueFor(this.index);
                }
                return this.rgb;
            }
        }

        static final class Value implements Color.Value {
            private static final String HEX_COLOR_REGEX = "([\\dA-Fa-f]{2})";
            private static final Pattern argbPattern = Pattern.compile(
                "^".concat(HEX_COLOR_REGEX).concat(HEX_COLOR_REGEX).concat(HEX_COLOR_REGEX).concat(HEX_COLOR_REGEX).concat("$")
            );
            private final PresetColorValues presetColorValues;
            private final String argb;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            Value(final PresetColorValues presetColorValues, final String argb) {
                this.presetColorValues = presetColorValues;
                this.argb = argb;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    final Matcher m = Value.argbPattern.matcher(this.argb);
                    if (!m.matches()) {
                        throw new IllegalArgumentException("Unsupported ARGB value: ".concat(this.argb));
                    }
                    final double alpha = (double) (Integer.parseInt(m.group(1), PercentageRgb.Value.RADIX)) / PercentageRgb.Value.MAX_PER_CHANNEL;
                    final int red = Integer.parseInt(m.group(2), PercentageRgb.Value.RADIX);
                    final int green = Integer.parseInt(m.group(3), PercentageRgb.Value.RADIX);
                    final int blue = Integer.parseInt(m.group(4), PercentageRgb.Value.RADIX);
                    this.rgb = String.format(
                        "%06X",
                        Math.round(alpha * red) << 16
                            | Math.round(alpha * green) << 8
                            | Math.round(alpha * blue)
                    );
                }
                return this.rgb;
            }

            String[] asHsl() {
                final String rgb = asRgb();
                final double[] percentages = new PercentageRgb.Value(this.presetColorValues, rgb).asPercentages();
                final double r = percentages[0] / PercentageRgb.Value.SCALE;
                final double g = percentages[1] / PercentageRgb.Value.SCALE;
                final double b = percentages[2] / PercentageRgb.Value.SCALE;
                final double cMin = Math.min(Math.min(r, g), b);
                final double cMax = Math.max(Math.max(r, g), b);
                final double c = cMax - cMin;
                final double h;
                if (0 == cMax) {
                    h = 0;
                } else if (r == cMax) {
                    h = (g - b) / c + (g < b ? 6 : 0);
                } else if (g == cMax) {
                    h = (b - r) / c + 2;
                } else {
                    // b == cMax
                    h = (r - g) / c + 4;
                }
                final double l = (cMin + cMax) / 2;
                final double s = c / (1 - Math.abs(2 * l - 1));
                return new String[] {
                    String.format("%d", Math.round(h * 60 * Hsl.Value.DEGREE_SCALE)),
                    String.format("%d", Math.round(s * PercentageRgb.Value.SCALE)),
                    String.format("%d", Math.round(l * PercentageRgb.Value.SCALE))
                };
            }
        }

        static final class ThemeValue implements Color.Value {
            private final PresetColorValues presetColorValues;
            private final Theme theme;
            private final int index;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            ThemeValue(final PresetColorValues presetColorValues, final Theme theme, final String index) {
                this(presetColorValues, theme, Integer.parseUnsignedInt(index));
            }

            ThemeValue(final PresetColorValues presetColorValues, final Theme theme, final int index) {
                this.presetColorValues = presetColorValues;
                this.theme = theme;
                this.index = index;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    this.rgb = this.theme.rgbColorValueFor(this.index);
                }
                return this.rgb;
            }
        }

        static final class ValueWithTint implements Color.Value {
            private final PresetColorValues presetColorValues;
            private final Value value;
            private final double tint;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            ValueWithTint(final PresetColorValues presetColorValues, final Value value, final String tint) {
                this(presetColorValues, value, Double.parseDouble(tint));
            }

            ValueWithTint(final PresetColorValues presetColorValues, final Value value, final double tint) {
                this.presetColorValues = presetColorValues;
                this.value = value;
                this.tint = tint;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    final String[] hsl = this.value.asHsl();
                    final double l = Double.parseDouble(hsl[2]) / PercentageRgb.Value.SCALE;
                    final String luminance;
                    if (0 == this.tint) {
                        luminance = hsl[2];
                    } else if (0 > this.tint) {
                        // Lum’ = Lum * (1.0 + tint)
                        luminance = String.format("%d", Math.round(l * (1.0 + this.tint) * PercentageRgb.Value.SCALE));
                    } else {
                        // 0 < t
                        // Lum‘ = Lum * (1.0 - tint) + (HLSMAX – HLSMAX * (1.0 - tint)), where HLSMAX = 255
                        // or 1.0 as in the current implementation
                        luminance = String.format(
                            "%d",
                            Math.round(
                                l * (1.0 - this.tint) + (1.0 - (1.0 - this.tint)) * PercentageRgb.Value.SCALE
                            )
                        );
                    }
                    this.rgb = new Hsl.Value(this.presetColorValues, hsl[0], hsl[1], luminance).asRgb();
                }
                return this.rgb;
            }
        }
    }

    final class Hsl implements Color {
        static final String NAME = "hslClr";
        private static final String HUE = "hue";
        private static final String LUMINANCE = "lum";
        private static final String SATURATION = "sat";
        private final PresetColorValues presetColorValues;
        private final Default defaultColor;
        private Color.Value value;

        Hsl(final PresetColorValues presetColorValues, final Default defaultColor) {
            this.presetColorValues = presetColorValues;
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                String hue = "";
                String luminance = "";
                String saturation = "";
                final Iterator iterator = this.defaultColor.startElement.getAttributes();
                while (iterator.hasNext()) {
                    final Attribute a = (Attribute) iterator.next();
                    switch (a.getName().getLocalPart()) {
                        case HUE:
                            hue = a.getValue();
                            break;
                        case LUMINANCE:
                            luminance = a.getValue();
                            break;
                        case SATURATION:
                            saturation = a.getValue();
                            break;
                        default:
                            throw new IllegalStateException("Unsupported attribute: ".concat(a.getName().getLocalPart()));
                    }
                }
                this.value = new Value(this.presetColorValues, hue, saturation, luminance);
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }

        static final class Value implements Color.Value {
            private static final int DEGREE_SCALE = 60_000;
            private final PresetColorValues presetColorValues;
            private final double hue;
            private final double saturation;
            private final double luminance;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            Value(final PresetColorValues presetColorValues, final String hue, final String saturation, final String luminance) {
                this(
                    presetColorValues, Double.parseDouble(hue) / Hsl.Value.DEGREE_SCALE,
                    Double.parseDouble(saturation) / PercentageRgb.Value.SCALE,
                    Double.parseDouble(luminance) / PercentageRgb.Value.SCALE
                );
            }

            Value(final PresetColorValues presetColorValues, final double hue, final double saturation, final double luminance) {
                this.presetColorValues = presetColorValues;
                this.hue = hue;
                this.saturation = saturation;
                this.luminance = luminance;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    final double a = this.saturation * Math.min(this.luminance, 1 - this.luminance);
                    this.rgb = new PercentageRgb.Value(
                        presetColorValues, colorChannelValueFor(this.hue, this.luminance, a, 0),
                        colorChannelValueFor(this.hue, this.luminance, a, 8),
                        colorChannelValueFor(this.hue, this.luminance, a, 4)
                    ).asRgb();
                }
                return this.rgb;
            }

            private static double colorChannelValueFor(final double h, final double l, final double a, final double n) {
                final double k = (n + h / 30) % 12;
                final double t1 = Math.min(k - 3, 9 - k);
                final double t2 = Math.min(t1, 1);
                return l - a * Math.max(-1, t2);
            }
        }
    }

    final class System implements Color {
        static final String NAME = "sysClr";
        static final String DEFAULT_BACKGROUND_NAME = "window";
        static final String DEFAULT_FOREGROUND_NAME = "windowText";
        private static final QName LAST_COLOR = new QName("lastClr");
        private final Default defaultColor;
        private Value value;

        System(final Default defaultColor) {
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                this.value = new Value(
                    this.defaultColor.value().asInternalName(),
                    this.defaultColor.startElement.getAttributeByName(LAST_COLOR).getValue()
                );
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }

        final static class Value implements Color.Value {
            static final String DEFAULT_BACKGROUND = "FFFFFF";
            static final String DEFAULT_FOREGROUND = "000000";
            private static final String EMPTY = "";
            private final String name;
            private final String rgb;

            Value(final String name, final String rgb) {
                this.name = name;
                this.rgb = rgb;
            }

            @Override
            public boolean matches(final String value) {
                return this.rgb.equals(value)
                    || this.name.equals(value);
            }

            @Override
            public String asInternalName() {
                return this.name;
            }

            @Override
            public String asExternalName() {
                return EMPTY;
            }

            @Override
            public String asRgb() {
                return this.rgb;
            }
        }
    }

    final class Scheme implements Color {
        static final String NAME = "schemeClr";
        private final PresetColorValues presetColorValues;
        private final ColorScheme colorScheme;
        private final Default defaultColor;
        private Color.Value value;

        Scheme(
            final PresetColorValues presetColorValues,
            final ColorScheme colorScheme,
            final Default defaultColor
        ) {
            this.presetColorValues = presetColorValues;
            this.colorScheme = colorScheme;
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                this.value = new Value(
                    this.presetColorValues,
                    this.colorScheme,
                    this.defaultColor.value().asInternalName() // index
                );
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }

        static final class Value implements Color.Value {
            private final PresetColorValues presetColorValues;
            private final ColorScheme colorScheme;
            private final int index;
            private Color.Value presetColorValue;
            private String internalName;
            private String externalName;
            private String rgb;

            Value(
                final PresetColorValues presetColorValues,
                final ColorScheme colorScheme,
                final String index
            ) {
                this(presetColorValues, colorScheme, Integer.parseUnsignedInt(index));
            }

            Value(
                final PresetColorValues presetColorValues,
                final ColorScheme colorScheme,
                final int index
            ) {
                this.presetColorValues = presetColorValues;
                this.colorScheme = colorScheme;
                this.index = index;
            }

            @Override
            public boolean matches(final String value) {
                final String rgb = asRgb();
                final Color.Value cv = presetColorValueFor(rgb);
                return rgb.equals(value)
                    || cv.asExternalName().equalsIgnoreCase(value)
                    || cv.asInternalName().equals(value);
            }

            private Color.Value presetColorValueFor(final String string) {
                if (null == this.presetColorValue) {
                    this.presetColorValue = this.presetColorValues.valueFor(string);
                }
                return this.presetColorValue;
            }

            @Override
            public String asInternalName() {
                if (null == this.internalName) {
                    this.internalName = presetColorValueFor(asRgb()).asInternalName();
                }
                return this.internalName;
            }

            @Override
            public String asExternalName() {
                if (null == this.externalName) {
                    this.externalName = presetColorValueFor(asRgb()).asExternalName();
                }
                return this.externalName;
            }

            @Override
            public String asRgb() {
                if (null == this.rgb) {
                    this.rgb =  this.colorScheme.colorValueFor(this.index);
                }
                return this.rgb;
            }
        }
    }

    final class Preset implements Color {
        static final String NAME = "prstClr";
        static final String HIGHLIGHT_NAME = "highlight";
        private final PresetColorValues presetColorValues;
        private final Color.Default defaultColor;
        private Color.Value value;

        Preset(final PresetColorValues presetColorValues, final Color.Default defaultColor) {
            this.presetColorValues = presetColorValues;
            this.defaultColor = defaultColor;
        }

        @Override
        public Color.Value value() {
            if (null == this.value) {
                this.value = this.presetColorValues.valueFor(
                    this.defaultColor.value().asInternalName() // name
                );
            }
            return this.value;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            this.defaultColor.readWith(reader);
        }

        @Override
        public Markup asMarkup() {
            return this.defaultColor.asMarkup();
        }
    }

    interface Value {
        boolean matches(final String value);
        String asInternalName();
        String asExternalName();
        String asRgb();

        final class Empty implements Value {
            private static final String EMPTY = "";

            @Override
            public String asInternalName() {
                return EMPTY;
            }

            @Override
            public String asExternalName() {
                return EMPTY;
            }

            @Override
            public String asRgb() {
                return EMPTY;
            }

            @Override
            public boolean matches(final String value) {
                return EMPTY.equals(value);
            }
        }
    }
}
