/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.XMLEventsReader;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.QuoteMode;
import net.sf.okapi.common.encoder.XMLEncoder;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.zip.ZipEntry;

class WorkbookPart extends TranslatablePart {
    private static final String EMPTY = "";
    private final WorkbookFragments fragments;
    private final IdGenerator textUnitIds;
    private final XMLEncoder attributeValueEncoder;
    private String subDocumentId;
    private Iterator<Event> filterEventsIterator;

    WorkbookPart(
        final Document.General generalDocument,
        final ZipEntry entry,
        final WorkbookFragments fragments
    ) {
        super(generalDocument, entry);
        this.fragments = fragments;
        this.textUnitIds = new IdGenerator(entry.getName(), IdGenerator.TEXT_UNIT);
        this.attributeValueEncoder = new XMLEncoder(OpenXMLFilter.ENCODING.name(), "\n", true, true, false, QuoteMode.ALL);
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.subDocumentId = this.generalDocument.nextSubDocumentId();
        readFilterEventsWith(new XMLEventsReader(this.fragments.events()));
        this.filterEventsIterator = this.filterEvents.iterator();
        return createStartSubDocumentEvent(this.generalDocument.documentId(), this.subDocumentId);
    }

    private void readFilterEventsWith(final XMLEventsReader reader) {
        while (reader.hasNext()) {
            final XMLEvent e = reader.nextEvent();
            if (!e.isStartElement() || !WorkbookFragments.SHEET.equals(e.asStartElement().getName().getLocalPart())) {
                addEventToDocumentPart(e);
                continue;
            }
            final String sheetName = e.asStartElement().getAttributeByName(WorkbookFragments.SHEET_NAME).getValue();
            if (this.fragments.localisedWorksheetNameHiddenFor(sheetName)) {
                addEventToDocumentPart(e);
                continue;
            }
            flushDocumentPart();
            final XMLEvent ee = reader.nextEvent();
            if (!ee.isEndElement() || !ee.asEndElement().getName().equals(e.asStartElement().getName())) {
                throw new IllegalStateException("Unsupported document structure found");
            }
            final CrossSheetCellReference crossSheetCellReference = new CrossSheetCellReference(
                sheetName,
                new CellReference(EMPTY)
            );
            final ITextUnit textUnit = textUnitFor(e.asStartElement(), crossSheetCellReference);
            this.generalDocument.dispersedTranslations().add(
                crossSheetCellReference,
                EMPTY,
                sheetName
            );
            this.filterEvents.add(new Event(EventType.TEXT_UNIT, textUnit));
        }
        flushDocumentPart();
        this.filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(this.subDocumentId)));
    }

    private ITextUnit textUnitFor(final StartElement startElement, final CrossSheetCellReference crossSheetCellReference) {
        final ITextUnit textUnit = new TextUnit(this.textUnitIds.createId());
        textUnit.setSource(new TextContainer(new TextFragment(crossSheetCellReference.worksheetName())));
        textUnit.setMimeType(OpenXMLFilter.MIME_TYPE);
        final GenericSkeleton skel = new GenericSkeleton();
        skel.append("<");
        skel.append(stringFor(startElement.getName()));
        final Iterator<?> iterator = startElement.getAttributes();
        while (iterator.hasNext()) {
            final Attribute attribute = (Attribute) iterator.next();
            skel.append(" ");
            skel.append(stringFor(attribute.getName()));
            skel.append("=\"");
            if (attribute.getValue().equals(crossSheetCellReference.worksheetName())) {
                skel.addContentPlaceholder(textUnit);
            } else {
                skel.append(stringFor(attribute.getValue()));
            }
            skel.append("\"");
        }
        skel.append("/>");
        textUnit.setSkeleton(skel);
        textUnit.setProperty(
            new Property(
                ExcelWorksheetTransUnitProperty.SHEET_NAME.getKeyName(),
                crossSheetCellReference.worksheetName(),
                true
            )
        );
        textUnit.setProperty(
            new Property(
                ExcelWorksheetTransUnitProperty.CELL_REFERENCE.getKeyName(),
                crossSheetCellReference.cellReference().toString(),
                true
            )
        );
        return textUnit;
    }

    private static String stringFor(final QName name) {
        final StringBuilder sb = new StringBuilder();
        if (null != name.getPrefix() && !"".equals(name.getPrefix())) {
            sb.append(name.getPrefix()).append(":");
        }
        sb.append(name.getLocalPart());
        return sb.toString();
    }

    private String stringFor(final String attributeValue) {
        return this.attributeValueEncoder.encode(attributeValue, EncoderContext.INLINE);
    }

    @Override
    public boolean hasNextEvent() {
        return this.filterEventsIterator.hasNext();
    }

    @Override
    public Event nextEvent() {
        return this.filterEventsIterator.next();
    }

    @Override
    public void close() {
    }

    @Override
    public void logEvent(final Event e) {
    }
}
