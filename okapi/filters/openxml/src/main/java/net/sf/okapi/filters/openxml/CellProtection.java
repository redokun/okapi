/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Arrays;
import java.util.LinkedList;

interface CellProtection {
    String NAME = "protection";
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements CellProtection {
        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements CellProtection {
        private final StartElement startElement;
        private EndElement endElement;

        Default(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final Markup markup = new Markup.General(new LinkedList<>());
            markup.addComponent(
                new MarkupComponent.General(
                    Arrays.asList(this.startElement, this.endElement)
                )
            );
            return markup;
        }
    }
}
