/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.filters.openxml.RunFonts.ContentCategory;
import net.sf.okapi.filters.openxml.RunProperty.FontsRunProperty;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

class RunMerger {

    /**
     * A message when unsupported run chunks provided.
     */
    private static final String UNSUPPORTED_RUN_CHUNKS_PROVIDED = "Unsupported run chunks provided";

    private String paragraphStyle;
    private RunBuilder runBuilder;
    private List<Chunk> completedRuns = new ArrayList<>();

    void setParagraphStyle(String paragraphStyle) {
        this.paragraphStyle = paragraphStyle;
    }

    boolean hasRunBuilder() {
        return runBuilder != null;
    }

    List<Chunk> getRuns() throws XMLStreamException {
        if (runBuilder != null) {
            completedRuns.add(runBuilder.build());
            runBuilder = null;
        }

        return completedRuns;
    }

    void add(final RunBuilder otherRunBuilder) throws XMLStreamException {
        if (null == runBuilder) {
            otherRunBuilder.resetCombinedRunProperties(paragraphStyle); // to be consistent with other run builders
            runBuilder = otherRunBuilder;

            return;
        }

        if (canMergeWith(otherRunBuilder)) {
            mergeWith(otherRunBuilder);
        } else {
            completedRuns.add(runBuilder.build());
            runBuilder = otherRunBuilder;
        }
    }

    private boolean canMergeWith(final RunBuilder otherRunBuilder) {
        if (runBuilder.isHidden() || otherRunBuilder.isHidden()){
            return false;
        }
        // Merging runs in the math namespace can sometimes corrupt formulas,
        // so don't do it.
        if (Namespaces.Math.containsName(runBuilder.getStartElementContext().getStartElement().getName())) {
            return false;
        }
        // XXX Don't merge runs that have nested blocks, to avoid having to go
        // back and renumber the references in the skeleton. I should probably
        // fix this at some point.  Note that we check for the existence of -any-
        // nested block, not just ones with text.
        // The reason for this pre-caution is because when we merge runs, we
        // re-parse the xml eventReader.  However, it doesn't cover all the cases.
        // We might be able to remove this restriction if we could clean up the
        // way the run body eventReader are parsed during merging.
        if (runBuilder.containsNestedItems() || otherRunBuilder.containsNestedItems()) {
            return false;
        }
        // Don't merge stuff involving complex codes
        if (runBuilder.containsComplexFields() || otherRunBuilder.containsComplexFields()) {
            return false;
        }
        return canRunPropertiesBeMerged(
            runBuilder.getCombinedRunProperties(paragraphStyle),
            otherRunBuilder.getCombinedRunProperties(paragraphStyle)
        );
    }

    private boolean canRunPropertiesBeMerged(RunProperties currentProperties, RunProperties otherProperties) {
        if (currentProperties.count() != otherProperties.count()) {
            return false;
        }

        int numberOfMatchedProperties = 0;

        for (final Property currentProperty : currentProperties.properties()) {
            QName currentPropertyStartElementName = currentProperty.getName();

            for (final Property otherProperty : otherProperties.properties()) {
                QName otherPropertyStartElementName = otherProperty.getName();

                if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
                    continue;
                }

                if (currentProperty instanceof MergeableRunProperty && otherProperty instanceof MergeableRunProperty) {
                    if (!((MergeableRunProperty) currentProperty).canBeMerged((MergeableRunProperty) otherProperty)) {
                        return false;
                    }
                } else {
                    if (currentProperty instanceof ReplaceableRunProperty && otherProperty instanceof ReplaceableRunProperty) {
                        if (!((ReplaceableRunProperty) currentProperty).canBeReplaced((ReplaceableRunProperty) otherProperty)) {
                            return false;
                        }
                    }
                }
                numberOfMatchedProperties++;
                break;
            }
        }

        if (numberOfMatchedProperties < currentProperties.count()) {
            return false;
        }

        return true;
    }

    /**
     * Merges the run builder with another run builder.
     *
     * That implies the merge of run properties and run body chunks.
     *
     * @param otherRunBuilder The other run builder to merge with
     */
    private void mergeWith(final RunBuilder otherRunBuilder) {

        runBuilder.setRunProperties(mergeRunProperties(runBuilder.getRunProperties(), otherRunBuilder.getRunProperties(),
                runBuilder.getCombinedRunProperties(paragraphStyle), otherRunBuilder.getCombinedRunProperties(paragraphStyle)));

        runBuilder.resetCombinedRunProperties(paragraphStyle);
        runBuilder.detectedRunFonts(mergedDetectedRunFonts(runBuilder.detectedRunFonts(), otherRunBuilder.detectedRunFonts()));

        runBuilder.setTextPreservingWhitespace(runBuilder.isTextPreservingWhitespace() || otherRunBuilder.isTextPreservingWhitespace());
        runBuilder.setRunBodyChunks(mergeRunBodyChunks(runBuilder.getRunBodyChunks(), otherRunBuilder.getRunBodyChunks()));
    }

    private RunProperties mergeRunProperties(RunProperties runProperties, RunProperties otherRunProperties,
                                             RunProperties combinedRunProperties, RunProperties otherCombinedRunProperties) {
        // try to reduce the set of properties
        final List<Property> mergeableRunProperties = runProperties.getMergeableRunProperties();
        final List<Property> otherMergeableRunProperties = otherRunProperties.getMergeableRunProperties();

        if (mergeableRunProperties.isEmpty() && otherMergeableRunProperties.isEmpty()) {
            return runProperties.count() <= otherRunProperties.count()
                    ? runProperties
                    : otherRunProperties;
        }

        if (mergeableRunProperties.size() >= otherMergeableRunProperties.size()) {
            final List<Property> remainedOtherMergeableRunProperties = mergeMergeableRunProperties(mergeableRunProperties, otherMergeableRunProperties);
            runProperties.refine(mergeableRunProperties);
            runProperties.properties().addAll(remainedOtherMergeableRunProperties);

            clarifyFontsRunProperties(runProperties, mergeCombinedRunProperties(combinedRunProperties, otherCombinedRunProperties));

            return runProperties;
        }

        final List<Property> remainedMergeableRunProperties = mergeMergeableRunProperties(otherMergeableRunProperties, mergeableRunProperties);
        otherRunProperties.refine(otherMergeableRunProperties);
        otherRunProperties.properties().addAll(remainedMergeableRunProperties);

        clarifyFontsRunProperties(otherRunProperties, mergeCombinedRunProperties(combinedRunProperties, otherCombinedRunProperties));

        return otherRunProperties;
    }

    private RunProperties mergeCombinedRunProperties(RunProperties combinedRunProperties, RunProperties otherCombinedRunProperties) {
        final List<Property> mergeableCombinedRunProperties = combinedRunProperties.getMergeableRunProperties();
        final List<Property> otherMergeableCombinedRunProperties = otherCombinedRunProperties.getMergeableRunProperties();

        if (mergeableCombinedRunProperties.size() >= otherMergeableCombinedRunProperties.size()) {
            final List<Property> remainedOtherMergeableCombinedRunProperties = mergeMergeableRunProperties(mergeableCombinedRunProperties, otherMergeableCombinedRunProperties);
            combinedRunProperties.refine(mergeableCombinedRunProperties);
            combinedRunProperties.properties().addAll(remainedOtherMergeableCombinedRunProperties);

            return combinedRunProperties;
        }

        final List<Property> remainedMergeableCombinedRunProperties = mergeMergeableRunProperties(otherMergeableCombinedRunProperties, mergeableCombinedRunProperties);
        otherCombinedRunProperties.refine(otherMergeableCombinedRunProperties);
        otherCombinedRunProperties.properties().addAll(remainedMergeableCombinedRunProperties);

        return otherCombinedRunProperties;
    }

    private List<Property> mergeMergeableRunProperties(List<Property> mergeableRunProperties, List<Property> otherMergeableRunProperties) {
        final List<Property> remainedOtherMergeableRunProperties = new ArrayList<>(otherMergeableRunProperties);

        final ListIterator<Property> mergeableRunPropertiesIterator = mergeableRunProperties.listIterator();
        while (mergeableRunPropertiesIterator.hasNext()) {
            final Property runProperty = mergeableRunPropertiesIterator.next();
            final QName currentPropertyStartElementName = runProperty.getName();

            final Iterator<Property> remainedOtherMergeableRunPropertyIterator = remainedOtherMergeableRunProperties.iterator();

            while (remainedOtherMergeableRunPropertyIterator.hasNext()) {
                final Property otherRunProperty = remainedOtherMergeableRunPropertyIterator.next();
                QName otherPropertyStartElementName = otherRunProperty.getName();

                if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
                    continue;
                }

                mergeableRunPropertiesIterator.set(
                    (Property) ((MergeableRunProperty) runProperty).merge((MergeableRunProperty) otherRunProperty)
                );
                remainedOtherMergeableRunPropertyIterator.remove();
                break;
            }
        }

        return remainedOtherMergeableRunProperties;
    }

    private void clarifyFontsRunProperties(RunProperties runProperties, RunProperties combinedRunProperties) {
        for (final Property combinedRunProperty : combinedRunProperties.properties()) {

            if (combinedRunProperty instanceof FontsRunProperty) {
                final ListIterator<Property> runPropertyIterator = runProperties.properties().listIterator();

                while (runPropertyIterator.hasNext()) {
                    final Property runProperty = runPropertyIterator.next();

                    if (runProperty instanceof FontsRunProperty) {
                        runPropertyIterator.set(combinedRunProperty);

                        return;
                    }
                }

                runPropertyIterator.add(combinedRunProperty);

                return;
            }
        }
    }

    private RunFonts mergedDetectedRunFonts(final RunFonts detectedRunFonts, final RunFonts otherDetectedRunFonts) {
        return detectedRunFonts.merge(otherDetectedRunFonts);
    }

    private List<Chunk> mergeRunBodyChunks(final List<Chunk> chunks, final List<Chunk> otherChunks) {
        if (chunks.isEmpty()) {
            return otherChunks;
        }
        if (otherChunks.isEmpty()) {
            return chunks;
        }

        final List<Chunk> mergedChunks = new ArrayList<>(chunks.size() + otherChunks.size());

        final ListIterator<Chunk> chunksIterator = chunks.listIterator(chunks.size() - 1);
        final ListIterator<Chunk> otherChunksIterator = otherChunks.listIterator(0);

        final Chunk chunk = chunksIterator.next();
        final Chunk otherChunk = otherChunksIterator.next();

        if (!canRunBodyChunksBeMerged(chunk, otherChunk)) {
            mergedChunks.addAll(chunks);
            mergedChunks.addAll(otherChunks);

            return mergedChunks;
        }

        if (-1 < chunksIterator.previousIndex()) {
            mergedChunks.addAll(chunks.subList(0, chunksIterator.previousIndex()));
        }

        mergedChunks.add(mergeRunBodyChunks(chunk, otherChunk));

        if (otherChunks.size() > otherChunksIterator.nextIndex()) {
            mergedChunks.addAll(otherChunks.subList(otherChunksIterator.nextIndex(), otherChunks.size()));
        }

        return mergedChunks;
    }

    private static boolean canRunBodyChunksBeMerged(final Chunk chunk, final Chunk otherChunk) {
        return chunk instanceof Run.Markup && otherChunk instanceof Run.Markup
                || chunk instanceof Run.RunText && otherChunk instanceof Run.RunText;
    }

    private Chunk mergeRunBodyChunks(final Chunk chunk, final Chunk otherChunk) {
        if (chunk instanceof Run.Markup && otherChunk instanceof Run.Markup) {
            return mergeRunMarkups((Run.Markup) chunk, (Run.Markup) otherChunk);
        }
        if (chunk instanceof Run.RunText && otherChunk instanceof Run.RunText) {
            return mergeRunTexts((Run.RunText) chunk, (Run.RunText) otherChunk);
        }
        throw new IllegalArgumentException(UNSUPPORTED_RUN_CHUNKS_PROVIDED);
    }

    private Run.Markup mergeRunMarkups(final Run.Markup markup, final Run.Markup otherMarkup) {
        final Run.Markup runMarkup = new Run.Markup(
            new Markup.General(
                new ArrayList<>(markup.components().size() + otherMarkup.components().size())
            )
        );
        runMarkup.addComponents(markup.components());
        runMarkup.addComponents(otherMarkup.components());

        return runMarkup;
    }

    private Chunk mergeRunTexts(final Run.RunText text, final Run.RunText otherText) {
        return new Run.RunText(
                mergeStartElements(text.startElement(), otherText.startElement()),
                mergeCharacters(text.characters(), otherText.characters()),
                text.endElement()
        );
    }

    private static StartElement mergeStartElements(final StartElement startElement, final StartElement otherStartElement) {
        return numberOfIterables(startElement.getAttributes()) >= numberOfIterables(otherStartElement.getAttributes())
                ? startElement
                : otherStartElement;
    }

    private static int numberOfIterables(Iterator iterator) {
        int number = 0;

        while (iterator.hasNext()) {
            iterator.next();
            number++;
        }

        return number;
    }

    private Characters mergeCharacters(final Characters characters, final Characters otherCharacters) {
        return runBuilder.getStartElementContext().getEventFactory()
                .createCharacters(characters.getData().concat(otherCharacters.getData()));
    }

    void reset() {
        completedRuns.clear();
        runBuilder = null;
    }

    /**
     * Adds text to the {@link Run.RunText} in the run builder.
     *
     * @param text The text to add
     */
    void addToRunTextInRunBuilder(String text) {
        runBuilder.addToFirstRunText(text);
    }
}
