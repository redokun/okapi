/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

interface Relationship {
    String NAME = "Relationship";
    QName ID = new QName("Id");
    QName TYPE = new QName("Type");
    QName TARGET = new QName("Target");
    String INTERNAL_TARGET_MODE = "Internal";
    String EXTERNAL_TARGET_MODE = "External";

    String id();
    String type();
    String target();
    String targetMode();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Default implements Relationship {
        private static final QName TARGET_MODE = new QName("TargetMode");

        private final XMLEventFactory eventFactory;
        private final Path basePath;
        private final StartElement startElement;
        private EndElement endElement;

        Default(
            final XMLEventFactory eventFactory,
            final Path basePath,
            final StartElement startElement
        ) {
            this.eventFactory = eventFactory;
            this.basePath = basePath;
            this.startElement = startElement;
        }

        @Override
        public String id() {
            return this.startElement.getAttributeByName(ID).getValue();
        }

        @Override
        public String type() {
            return this.startElement.getAttributeByName(TYPE).getValue();
        }

        @Override
        public String target() {
            final String target = this.startElement.getAttributeByName(TARGET).getValue();
            final String resolvedTarget;
            if (Relationship.INTERNAL_TARGET_MODE.equals(targetMode())) {
                if (target.startsWith("/")) {
                    resolvedTarget =
                        this.basePath.resolve(
                            this.basePath.relativize(Paths.get(target.substring(1)))
                        ).normalize().toString();
                } else {
                    resolvedTarget = this.basePath.resolve(target).normalize().toString();
                }
            } else {
                resolvedTarget = target;
            }
            return resolvedTarget;
        }

        @Override
        public String targetMode() {
            final Attribute a = this.startElement.getAttributeByName(TARGET_MODE);
            return null == a
                ? INTERNAL_TARGET_MODE
                : a.getValue();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
            }
        }

        @Override
        public Markup asMarkup() {
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new ArrayList<>()));
            mb.add(this.startElement);
            mb.add(endElement());
            return mb.build();
        }

        private EndElement endElement() {
            if (null == this.endElement) {
                this.endElement = this.eventFactory.createEndElement(
                    this.startElement.getName(),
                    this.startElement.getNamespaces()
                );
            }
            return this.endElement;
        }
    }
}
