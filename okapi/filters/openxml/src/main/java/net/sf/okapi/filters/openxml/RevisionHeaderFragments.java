/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.HashSet;
import java.util.Set;

interface RevisionHeaderFragments {
    String revisionLogId();
    boolean reviewed(final Set<String> revisionIds);
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;

    final class Default implements RevisionHeaderFragments {
        private static final String EMPTY = "";
        private static final QName MIN_R_ID = new QName("minRId");
        private static final QName MAX_R_ID = new QName("maxRId");
        private static final String REVIEWED = "reviewed";
        private static final QName R_ID = new QName("rId");
        private final StartElement startElement;
        private final QName idName;
        private final Set<String> reviewedRevisionIds;
        private String revisionLogId;

        Default(final StartElement startElement, final QName idName) {
            this(
                startElement,
                idName,
                new HashSet<>()
            );
        }

        Default(
            final StartElement startElement,
            final QName idName,
            final Set<String> reviewedRevisionIds
        ) {
            this.startElement = startElement;
            this.idName = idName;
            this.reviewedRevisionIds = reviewedRevisionIds;
        }

        @Override
        public String revisionLogId() {
            return this.revisionLogId;
        }

        @Override
        public boolean reviewed(final Set<String> revisionIds) {
            return this.reviewedRevisionIds.equals(revisionIds);
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            this.revisionLogId = XMLEventHelpers.getAttributeValue(this.startElement, this.idName, EMPTY);
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    return;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Default.REVIEWED.equals(se.getName().getLocalPart())) {
                    this.reviewedRevisionIds.add(XMLEventHelpers.getAttributeValue(se, R_ID));
                }
            }
        }

//        private void fillInRevisionIds() {
//            final String min = XMLEventHelpers.getAttributeValue(
//                this.startElement,
//                Default.MIN_R_ID,
//                Default.EMPTY
//            );
//            final String max = XMLEventHelpers.getAttributeValue(
//                this.startElement,
//                Default.MAX_R_ID,
//                Default.EMPTY
//            );
//            if (!min.isEmpty() && !max.isEmpty()) {
//                final int startId = Integer.parseUnsignedInt(min);
//                final int endId = Integer.parseUnsignedInt(max);
//                this.revisionIds.addAll(
//                    IntStream.rangeClosed(startId, endId)
//                        .boxed()
//                        .map(i -> String.valueOf(i))
//                        .collect(Collectors.toSet())
//                );
//            } else if (!min.isEmpty()) {
//                this.revisionIds.add(min);
//            } else if (!max.isEmpty()) {
//                this.revisionIds.add(max);
//            }
//        }
    }
}
