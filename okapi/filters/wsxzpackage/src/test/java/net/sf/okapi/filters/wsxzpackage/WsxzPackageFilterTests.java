/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.wsxzpackage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;
import java.util.List;

import net.sf.okapi.filters.wsxzpackage.WsxzPackageFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;

@RunWith(JUnit4.class)
public class WsxzPackageFilterTests {

	private final LocaleId locEN = LocaleId.fromString("en-US");
	private final LocaleId locES = LocaleId.fromString("es-ES");

	private FileLocation root;
	
	@Before
	public void setUp()	{
		root = FileLocation.fromClass(this.getClass());
	}

	@Test
	public void testInformation () {
		try ( WsxzPackageFilter filter = new WsxzPackageFilter() ) {
			
			assertEquals(WsxzPackageFilter.MIME_TYPE, filter.getMimeType());
			assertEquals("okf_wsxzpackage", filter.getName());
			assertEquals("WSXZ Filter (BETA)", filter.getDisplayName());
			
			List<FilterConfiguration> confs = filter.getConfigurations();
			assertEquals(1, confs.size());
			FilterConfiguration conf = confs.get(0);
			assertEquals(WsxzPackageFilter.class.getName(), conf.filterClass);
			assertEquals(filter.getName(), conf.configId);
		}
	}
	
	@Test
	public void testSimpleRead () {
		try ( WsxzPackageFilter filter = new WsxzPackageFilter() ) {
			URI uri = root.in("/test1.wsxz").asUri();
			RawDocument rd = new RawDocument(uri, "UTF-8", locEN, locES);
			filter.setOptions(locEN, locES, "UTF-8", true);
			filter.open(rd);
			int sdCount = 0;
			int segCount = 0;
			boolean segFound = false;
			String sdFound = null;
			while ( filter.hasNext() ) {
				Event event = filter.next();
				switch ( event.getEventType() ) {
				case START_SUBDOCUMENT:
					sdCount++; // Note that XLIFF file is seen as a sub-document too
					if ( sdCount == 2 ) {
						sdFound = event.getStartSubDocument().getName();
					}
					break;
				case TEXT_UNIT:
					ITextUnit tu = event.getTextUnit();
					ISegments segs = tu.getSource().getSegments();
					for ( Segment seg : segs ) {
						segCount++;
						if ( seg.getContent().getCodedText().equals(
							"Applications") ) {
							segFound = true;
						}
					}
				default:
					break;
				}
			}
			assertEquals(2, sdCount);
			assertEquals("F:\\Curriculum\\HTML\\Source\\okapi_intro_test.html", sdFound);
			assertEquals(15, segCount);
			assertTrue(segFound);
		}
	}
	
	@Test
	public void testSimpleReadWrite () {
		IFilterWriter writer = null;
		File out = root.in("/test1.out.wsxz").asFile();
		out.delete();
		assertFalse(out.exists());

		// Read and write with target in capital letters
		try ( WsxzPackageFilter filter = new WsxzPackageFilter() ) {
			URI uri = root.in("/test1.wsxz").asUri();
			RawDocument rd = new RawDocument(uri, "UTF-8", locEN, locES);
			filter.setOptions(locEN, locES, "UTF-8", true);
			filter.open(rd);
			// Prepare writer
			writer = filter.createFilterWriter();
			writer.setOutput(out.getAbsolutePath());
			
			while ( filter.hasNext() ) {
				Event event = filter.next();
				if ( event.isTextUnit() ) {
					ITextUnit tu = event.getTextUnit();
					// SDLX has empty <target> elements, so we force the copy
					TextContainer tc = tu.createTarget(locES, true, IResource.COPY_ALL);
					ISegments segs = tc.getSegments();
					for ( Segment seg : segs ) {
						TextFragment tf = seg.getContent();
						tf.setCodedText(tf.getCodedText().toUpperCase());
					}
				}
				writer.handleEvent(event);
			}
		}
		finally {
			if ( writer != null ) {
				writer.close();
			}
		}

		// Read and check the output
		try ( WsxzPackageFilter filter = new WsxzPackageFilter() ) {
			URI uri = root.in("/test1.out.wsxz").asUri();
			RawDocument rd = new RawDocument(uri, "UTF-8", locEN, locES);
			filter.setOptions(locEN, locES, "UTF-8", true);
			filter.open(rd);
			
			while ( filter.hasNext() ) {
				Event event = filter.next();
				if ( event.isTextUnit() ) {
					ITextUnit tu = event.getTextUnit();
					TextContainer tc = tu.getTarget(locES);
					assertNotNull(tc);
					ISegments segs = tc.getSegments();
					for ( Segment seg : segs ) {
						String found = seg.getContent().getCodedText();
						String expected = seg.getContent().getCodedText().toUpperCase();
						assertEquals(expected, found);
					}
				}
			}
		}
	}
}
