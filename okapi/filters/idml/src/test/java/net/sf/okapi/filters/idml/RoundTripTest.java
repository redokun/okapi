/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.EventType;
import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.FileLocation.In;
import net.sf.okapi.common.FileLocation.Out;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.TextContainer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class RoundTripTest {
    private static final LocaleId DEFAULT_LOCALE = LocaleId.ENGLISH;
    private static final String DEFAULT_CHARSET = StandardCharsets.UTF_8.name();
    private FileLocation root;

    @Before
    public void setUp() {
        root = FileLocation.fromClass(this.getClass());
    }

    @Test
    public void documentWithChainedFontMappings() {
        final Parameters parameters = new Parameters();
        parameters.fromString(
            "#v1\n" +
            "fontMappings.number.i=3\n" +
            "fontMappings.0.sourceFontPattern=Times.*\n" +
            "fontMappings.0.targetFont=Arial Unicode MS\n" +
            "fontMappings.1.targetLocalePattern=en\n" +
            "fontMappings.1.sourceFontPattern=The Sims Sans\n" +
            "fontMappings.1.targetFont=Arial Unicode MS\n" +
            "fontMappings.2.targetLocalePattern=en\n" +
            "fontMappings.2.sourceFontPattern=Arial Unicode MS\n" +
            "fontMappings.2.targetFont=Meiryo\n"
        );
        roundTripAndCheck(parameters, "926.idml", "926-chained.idml");
    }

    @Test
    public void documentsWithDefaultParameters() {
        roundTripAndCheck(new Parameters(), "926.idml", "926.idml");
    }

    @Test
    public void emptyTargetsMerged() {
        final FileLocation.In input = root.in("/629.idml");
        final FileLocation.Out actualOutput = root.out("/actual/629.idml");
        final FileLocation.In expectedOutput = root.in("/expected/629.idml");
        try (final RawDocument rawDocument = new RawDocument(input.asUri(), DEFAULT_CHARSET, DEFAULT_LOCALE);
             final IDMLFilter filter = new IDMLFilter()) {
            filter.setParameters(new Parameters());
            filter.open(rawDocument, true);
            try (final IFilterWriter filterWriter = filter.createFilterWriter()) {
                filterWriter.setOutput(actualOutput.toString());
                filterWriter.setOptions(LocaleId.FRENCH, StandardCharsets.UTF_8.name());
                filter.forEachRemaining(event -> {
                    if (EventType.TEXT_UNIT.equals(event.getEventType())) {
                        final ITextUnit tu = event.getTextUnit();
                        if ("Second paragraph.".equals(tu.getSource().getCodedText())
                            || "Last paragraph.".equals(tu.getSource().getCodedText())) {
                            event.getTextUnit().setTarget(LocaleId.FRENCH, new TextContainer());
                        }
                    }
                    filterWriter.handleEvent(event);
                });
            }
        }
        final ZipXMLFileCompare zfc = new ZipXMLFileCompare();
        assertTrue(zfc.compareFiles(actualOutput.toString(), expectedOutput.toString()));
    }

    @Test
    public void specialCharactersExtractedAndMerged() {
        roundTripAndCheck(new Parameters(), "175-special-characters.idml", "175-special-characters.idml");
    }

    @Test
    public void customTextVariablesExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractCustomTextVariables(true);
        roundTripAndCheck(parameters, "1138.idml", "1138.idml");
    }

    @Test
    public void indexTopicsExtractedAndMerged() {
        final Parameters parameters = new Parameters();
        parameters.setExtractIndexTopics(true);
        roundTripAndCheck(parameters, "1139.idml", "1139.idml");
    }

    @Test
    public void endNotesExtractedAndMerged() {
        roundTripAndCheck(new Parameters(), "856-1.idml", "856-1.idml");
        roundTripAndCheck(new Parameters(), "856-2.idml", "856-2.idml");
    }

    private void roundTripAndCheck(final Parameters parameters, final String documentName, final String goldDocumentName) {
        final In input = root.in("/" + documentName);
        final Out actualOutput = root.out("/actual/" + goldDocumentName);
        final In expectedOutput = root.in("/expected/" + goldDocumentName);
		try (final RawDocument rawDocument = new RawDocument(input.asUri(), DEFAULT_CHARSET, DEFAULT_LOCALE);
				final IDMLFilter filter = new IDMLFilter()) {
			filter.setParameters(parameters);
			filter.open(rawDocument, true);
			try (final IFilterWriter filterWriter = filter.createFilterWriter()) {
				filterWriter.setOutput(actualOutput.toString());
				filterWriter.setOptions(DEFAULT_LOCALE, DEFAULT_CHARSET);
				filter.forEachRemaining(filterWriter::handleEvent);
			}
		}
		final ZipXMLFileCompare zfc = new ZipXMLFileCompare();
		assertTrue(zfc.compareFiles(actualOutput.toString(), expectedOutput.toString()));
	}
}
