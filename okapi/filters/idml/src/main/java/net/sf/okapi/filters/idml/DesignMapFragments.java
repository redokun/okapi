/*
 * =============================================================================
 *   Copyright (C) 2010-2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static net.sf.okapi.filters.idml.OrderingIdioms.getOrderedPasteboardItems;
import static net.sf.okapi.filters.idml.OrderingIdioms.getOrderedStoryIds;
import static net.sf.okapi.filters.idml.OrderingIdioms.getOrderedStoryPartNames;
import static net.sf.okapi.filters.idml.ParsingIdioms.SELF;
import static net.sf.okapi.filters.idml.ParsingIdioms.VISIBLE;

interface DesignMapFragments {
    String MIME_TYPE = "mimetype";
    String DESIGN_MAP = "designmap.xml";

    String stylesPartName();
    List<String> orderedPartNames() throws XMLStreamException, IOException;
    List<String> translatablePartNames() throws XMLStreamException, IOException;
    List<XMLEvent> events();
    void from(final ZipEntry zipEntry) throws XMLStreamException, IOException;

    final class Default implements DesignMapFragments {
        private static final String CONTAINER = "META-INF/container.xml";
        private static final String METADATA = "META-INF/metadata.xml";

        private static final QName DOCUMENT = Namespaces.getDefaultNamespace().getQName("Document");
        private static final QName ACTIVE_LAYER = Namespaces.getDefaultNamespace().getQName("ActiveLayer");

        private static final QName SRC = Namespaces.getDefaultNamespace().getQName("src");
        private static final QName VARIABLE_TYPE = Namespaces.getDefaultNamespace().getQName("VariableType");

        private static final QName GRAPHIC = Namespaces.getIdPackageNamespace().getQName("Graphic");
        private static final QName FONTS = Namespaces.getIdPackageNamespace().getQName("Fonts");
        private static final QName STYLES = Namespaces.getIdPackageNamespace().getQName("Styles");
        private static final QName PREFERENCES = Namespaces.getIdPackageNamespace().getQName("Preferences");
        private static final QName TEXT_VARIABLE = Namespaces.getDefaultNamespace().getQName("TextVariable");
        private static final String CUSTOM_TEXT_TYPE = "CustomTextType";
        private static final QName TAGS = Namespaces.getIdPackageNamespace().getQName("Tags");
        private static final QName MAPPING = Namespaces.getIdPackageNamespace().getQName("Mapping");

        private static final QName LAYER = Namespaces.getDefaultNamespace().getQName("Layer");

        private static final QName MASTER_SPREAD = Namespaces.getIdPackageNamespace().getQName("MasterSpread");
        private static final QName SPREAD = Namespaces.getIdPackageNamespace().getQName("Spread");

        static final QName MASTER_SPREAD_DEFAULT = Namespaces.getDefaultNamespace().getQName("MasterSpread");
        static final QName SPREAD_DEFAULT = Namespaces.getDefaultNamespace().getQName("Spread");

        private static final QName INDEX = Namespaces.getDefaultNamespace().getQName("Index");
        private static final QName TOPIC = Namespaces.getDefaultNamespace().getQName("Topic");

        private static final QName BACKING_STORY = Namespaces.getIdPackageNamespace().getQName("BackingStory");
        private static final QName STORY = Namespaces.getIdPackageNamespace().getQName("Story");


        private final Parameters parameters;
        private final XMLEventFactory eventFactory;
        private final ZipFile zipFile;
        private final ZipInput<XMLEventReader> zipInputReader;
        private String activeLayerId;
        private String graphicPartName;
        private String fontsPartName;
        private String stylesPartName;
        private String preferencesPartName;
        private String tagsPartName;
        private String mappingPartName;
        private final List<Layer> layers;
        private final List<String> masterSpreadPartNames;
        private final List<String> spreadPartNames;
        private String backingStoryPartName;
        private final List<String> storyPartNames;
        private final List<XMLEvent> events;
        private List<String> visibleStoryPartNames;

        Default(
            final Parameters parameters,
            final XMLEventFactory eventFactory,
            final ZipFile zipFile,
            final ZipInput<XMLEventReader> zipInputReader) {
            this(
                parameters,
                eventFactory,
                zipFile,
                zipInputReader,
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
            );
        }

        Default(
            final Parameters parameters,
            final XMLEventFactory eventFactory,
            final ZipFile zipFile,
            final ZipInput<XMLEventReader> zipInputReader,
            final List<Layer> layers,
            final List<String> masterSpreadPartNames,
            final List<String> spreadPartNames,
            final List<String> storyPartNames,
            final List<XMLEvent> events
        ) {
            this.parameters = parameters;
            this.eventFactory = eventFactory;
            this.zipFile = zipFile;
            this.zipInputReader = zipInputReader;
            this.layers = layers;
            this.masterSpreadPartNames = masterSpreadPartNames;
            this.spreadPartNames = spreadPartNames;
            this.storyPartNames = storyPartNames;
            this.events = events;
        }

        @Override
        public String stylesPartName() {
            return this.stylesPartName;
        }

        @Override
        public List<String> orderedPartNames() throws XMLStreamException, IOException {
            final List<String> partNames = new LinkedList<>();
            partNames.add(MIME_TYPE);
            partNames.add(DESIGN_MAP);
            partNames.add(CONTAINER);
            partNames.add(METADATA);
            partNames.add(this.graphicPartName);
            partNames.add(this.fontsPartName);
            partNames.add(this.stylesPartName);
            partNames.add(this.preferencesPartName);
            partNames.add(this.tagsPartName);
            partNames.add(this.mappingPartName);
            partNames.addAll(this.masterSpreadPartNames);
            partNames.addAll(this.spreadPartNames);
            partNames.add(this.backingStoryPartName);
            partNames.addAll(visibleStoryPartNames());
            return partNames;
        }

        private List<String> visibleStoryPartNames() throws XMLStreamException, IOException {
            if (null == this.visibleStoryPartNames) {
                Preferences preferences = new PreferencesParser(this.zipInputReader).parse(this.zipFile.getEntry(this.preferencesPartName));
                final List<PasteboardItem> pasteboardItems = new ArrayList<>();
                final List<PasteboardItem> invisiblePasteboardItems = new ArrayList<>();

                final List<Spread> masterSpreads = spreads(this.masterSpreadPartNames, this.activeLayerId, MASTER_SPREAD_DEFAULT);
                final List<PasteboardItem> masterSpreadPasteboardItems = getOrderedPasteboardItems(
                    masterSpreads,
                    preferences.getStoryPreference().getStoryDirection(),
                    eventFactory
                );

                if (parameters.getExtractMasterSpreads()) {
                    pasteboardItems.addAll(masterSpreadPasteboardItems);
                } else {
                    invisiblePasteboardItems.addAll(masterSpreadPasteboardItems);
                }

                final List<Spread> spreads = spreads(this.spreadPartNames, this.activeLayerId, SPREAD_DEFAULT);
                pasteboardItems.addAll(getOrderedPasteboardItems(spreads, preferences.getStoryPreference().getStoryDirection(), eventFactory));

                final List<PasteboardItem> visiblePasteboardItems = visiblePasteboardItems(pasteboardItems);
                invisiblePasteboardItems.addAll(invisiblePasteboardItems(pasteboardItems, visiblePasteboardItems));

                final List<String> visibleStoryIds = getOrderedStoryIds(visiblePasteboardItems);
                this.visibleStoryPartNames = getOrderedStoryPartNames(this.storyPartNames, visibleStoryIds);

                final List<String> invisibleStoryIds = getOrderedStoryIds(invisiblePasteboardItems);
                final List<String> invisibleStoryPartNames = getOrderedStoryPartNames(this.storyPartNames, invisibleStoryIds);

                final List<String> anchoredStoryPartNames = this.storyPartNames.stream()
                    .filter(s -> !this.visibleStoryPartNames.contains(s) && !invisibleStoryPartNames.contains(s))
                    .collect(Collectors.toList());
                this.visibleStoryPartNames.addAll(anchoredStoryPartNames);
            }
            return this.visibleStoryPartNames;
        }

        private List<Spread> spreads(List<String> spreadPartNames, String activeLayerId, QName spreadName) throws IOException, XMLStreamException {
            List<Spread> spreads = new ArrayList<>();
            final SpreadParser sp = new SpreadParser(this.zipInputReader, spreadName, eventFactory, activeLayerId);
            for (String spreadPartName : spreadPartNames) {
                Spread spread = sp.parse(zipFile.getEntry(spreadPartName));
                spreads.add(spread);
            }

            return spreads;
        }

        private List<PasteboardItem> visiblePasteboardItems(final List<PasteboardItem> pasteboardItems) {
            return new PasteboardItem.VisibilityFilter(
                this.layers,
                parameters.getExtractHiddenLayers(),
                parameters.getExtractHiddenPasteboardItems()
            ).filterVisible(pasteboardItems);
        }

        private List<PasteboardItem> invisiblePasteboardItems(List<PasteboardItem> pasteboardItems, List<PasteboardItem> visiblePasteboardItems) {
            List<PasteboardItem> invisiblePasteboardItems = new ArrayList<>(pasteboardItems);
            invisiblePasteboardItems.removeAll(visiblePasteboardItems);
            return invisiblePasteboardItems;
        }

        @Override
        public List<String> translatablePartNames() throws XMLStreamException, IOException {
            final List<String> partNames = new LinkedList<>();
            if (!this.events.isEmpty()) {
                partNames.add(DESIGN_MAP);
            }
            partNames.addAll(visibleStoryPartNames());
            return partNames;
        }

        @Override
        public List<XMLEvent> events() {
            return this.events;
        }

        @Override
        public void from(final ZipEntry zipEntry) throws XMLStreamException, IOException {
            final XMLEventReader eventReader = this.zipInputReader.of(zipEntry);
            this.layers.clear();
            this.masterSpreadPartNames.clear();
            this.spreadPartNames.clear();
            this.storyPartNames.clear();
            this.events.clear();
            boolean translatable = false;
            boolean inIndex = false;

            while (eventReader.hasNext()) {
                final XMLEvent event = eventReader.nextEvent();
                this.events.add(event);
                if (event.isEndElement() && INDEX.equals(event.asEndElement().getName())) {
                    inIndex = false;
                    continue;
                }
                if (!event.isStartElement()) {
                    continue;
                }
                final StartElement se = event.asStartElement();
                if (DOCUMENT.equals(se.getName())) {
                    this.activeLayerId = se.getAttributeByName(ACTIVE_LAYER).getValue();
                    continue;
                }
                if (GRAPHIC.equals(se.getName())) {
                    this.graphicPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (FONTS.equals(se.getName())) {
                    this.fontsPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (STYLES.equals(se.getName())) {
                    this.stylesPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (PREFERENCES.equals(se.getName())) {
                    this.preferencesPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (TEXT_VARIABLE.equals(se.getName())) {
                    final String type = se.getAttributeByName(VARIABLE_TYPE).getValue();
                    if (CUSTOM_TEXT_TYPE.equals(type) && this.parameters.getExtractCustomTextVariables()) {
                        translatable = true;
                    }
                    continue;
                }
                if (TAGS.equals(se.getName())) {
                    this.tagsPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (MAPPING.equals(se.getName())) {
                    this.mappingPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (LAYER.equals(se.getName())) {
                    this.layers.add(
                        new Layer(
                            se.getAttributeByName(SELF).getValue(),
                            Boolean.parseBoolean(se.getAttributeByName(VISIBLE).getValue())
                        )
                    );
                    continue;
                }
                if (MASTER_SPREAD.equals(se.getName())) {
                    this.masterSpreadPartNames.add(se.getAttributeByName(SRC).getValue());
                    continue;
                }
                if (SPREAD.equals(se.getName())) {
                    this.spreadPartNames.add(se.getAttributeByName(SRC).getValue());
                    continue;
                }
                if (INDEX.equals(se.getName())) {
                    inIndex = true;
                    continue;
                }
                if (inIndex && this.parameters.getExtractIndexTopics() && TOPIC.equals(se.getName())) {
                    translatable = true;
                    continue;
                }
                if (BACKING_STORY.equals(se.getName())) {
                    this.backingStoryPartName = se.getAttributeByName(SRC).getValue();
                    continue;
                }
                if (STORY.equals(se.getName())) {
                    this.storyPartNames.add(se.getAttributeByName(SRC).getValue());
                }
            }
            if (!translatable) {
                this.events.clear();
            }
        }
    }
}
